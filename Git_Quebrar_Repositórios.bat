rem Ver https://confluence.atlassian.com/display/BITBUCKET/Split+a+repository+in+two

rem Remove the link back to the remote repository
git remote rm origin

rem Remove anything from the repository that is not to be there
git filter-branch --index-filter "git rm --cached --ignore-unmatch -r BoletadorImporter" -- --all

rem Add to BitBucket remote repository
git remote add origin https://varantes@bitbucket.org/varantes/boletadorposativos.git