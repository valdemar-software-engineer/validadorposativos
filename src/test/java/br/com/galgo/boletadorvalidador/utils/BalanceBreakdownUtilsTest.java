/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import jodd.bean.BeanUtil;
import org.assertj.core.api.Assertions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;

/**
 * @author valdemar.arantes
 */
public class BalanceBreakdownUtilsTest {

    private SubBalanceInformation6 balBrkDwn;
    private BalanceBreakdownUtils utils;

    public BalanceBreakdownUtilsTest() {
    }

    @BeforeMethod
    public void beforeMethod() {
        balBrkDwn = new SubBalanceInformation6();
        utils = new BalanceBreakdownUtils(balBrkDwn, null);
    }

    @Test
    public void getCodigoTipo() {
        BeanUtil.setPropertyForced(balBrkDwn, "subBalTp.cd", SecuritiesBalanceType12Code.PEND);
        Assertions.assertThat(utils.getCodigoTipo()).isEqualTo(SecuritiesBalanceType12Code.PEND);
    }

    @Test
    public void getQtdCotas() {
        BeanUtil.setPropertyForced(balBrkDwn, "qty.qty.unit", BigDecimal.TEN);
        Assertions.assertThat(utils.getQtdCotas()).isEqualTo(BigDecimal.TEN);
    }

    @Test
    void getValorFinanceiro1() {
        BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[0].qty.qty.faceAmt", BigDecimal.TEN);
        BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[1].qty.qty.faceAmt", BigDecimal.ONE);
        Assertions.assertThat(utils.getValorFinanceiro()).isEqualTo(BigDecimal.TEN.add(BigDecimal.ONE));
    }

    @Test
    void getValorFinanceiro2() {
        BeanUtil.setPropertyForced(balBrkDwn, "subBalTp.cd", SecuritiesBalanceType12Code.PEND);
        Assertions.assertThat(utils.getValorFinanceiro()).isNull();

        BeanUtil.setPropertyForced(balBrkDwn, "qty.qty.faceAmt", BigDecimal.TEN);
        Assertions.assertThat(utils.getValorFinanceiro()).isEqualTo(BigDecimal.TEN);
    }


}
