/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador;

import br.com.galgo.boletadorvalidador.exporters.TemplateExcelExporter;
import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.lang.SystemUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class TemplateExcelExporterTest {

    private static final Logger log = LoggerFactory.getLogger(
            TemplateExcelExporterTest.class);

    @Test
    public void exportFileTest() {
        try {
            String filename = SystemUtils.getUserHome() + "/teste.xls";
            File outFile = new File(filename);
            if (outFile.exists()) {
                if (outFile.isDirectory()) {
                    Assertions.fail(filename + " não pode ser uma pasta. Reescrever o teste"
                            + " utilizando outro nome para a"
                            + " planilha a ser exportada");
                } else {
                    if (!outFile.delete()) {
                        Assertions.fail("Não foi possível apagar o arguivo " + filename
                                + ". Remova-o e reexecute este teste");
                    }
                }
            }

            new TemplateExcelExporter(outFile).exportFile();
            Assertions.assertThat(outFile).exists().isFile();
            Workbook wb = new HSSFWorkbook(new FileInputStream(outFile), false);
            String firstSheetName = "Fundo";
            Assertions.assertThat(wb.getSheetName(0)).isEqualTo(firstSheetName);
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }
}
