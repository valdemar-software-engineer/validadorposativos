package br.com.galgo.boletadorvalidador;

import br.com.galgo.boletadorvalidador.utils.ObjectUtils;
import br.com.galgo.boletadorvalidador.utils.XMLUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.ObjectFactory;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Unit test for simple App.
 */
public class AppTest {

    private static final Logger log = LoggerFactory.getLogger(AppTest.class);

    /**
     * Rigourous Test :-)
     */
    @Test
    public void marshalling() {
        System.out.println("\n\n*** marshalling   **************");
        ObjectFactory objFac = new ObjectFactory();
        Document document = objFac.createDocument();
        SecuritiesBalanceAccountingReportV04 secBalAccRprtV04 = objFac.
                createSecuritiesBalanceAccountingReportV04();
        document.setSctiesBalAcctgRpt(secBalAccRprtV04);
        JAXBElement<Document> rootElem = objFac.createDocument(document);

        System.out.println("rootElem: " + rootElem.getClass().getName());
        try {
            JAXBContext ctx = JAXBContext.newInstance(rootElem.getValue().getClass());
            Marshaller m = ctx.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(rootElem, System.out);
        } catch (JAXBException e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }

    }

    @Test
    public void unmarshalling() {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                + "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:semt.003.001.04\">\n"
                + "    <SctiesBalAcctgRpt/>\n"
                + "</Document>";
        System.out.println("\n\n*** unmarshalling   **************");

        try {
            org.w3c.dom.Document xmlDoc = XMLUtils.buildXml(new StringReader(xml));
            JAXBContext ctx = JAXBContext.newInstance(Document.class);
            Unmarshaller unm = ctx.createUnmarshaller();

            SAXSource source = new SAXSource(new InputSource(new StringReader(xml)));

            JAXBElement<Document> jaxbDoc = unm.unmarshal(source, Document.class);
            Document document = jaxbDoc.getValue();
            log.debug("document=\n" + ObjectUtils.getAsJsonString(document, true));
            Assertions.assertThat(document).isNotNull();
            Assertions.assertThat(document.getSctiesBalAcctgRpt()).isNotNull();
        } catch (ParserConfigurationException | SAXException | IOException | JAXBException e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }

    }

    @Test
    public void validateObject() {
        System.out.println("\n\n*** validateObject   **************");
        ObjectFactory objFac = new ObjectFactory();
        Document document = objFac.createDocument();
        SecuritiesBalanceAccountingReportV04 secBalAccRprtV04 = objFac.
                createSecuritiesBalanceAccountingReportV04();
        document.setSctiesBalAcctgRpt(secBalAccRprtV04);
        try {
            JAXBContext ctx = JAXBContext.newInstance(Document.class);

            JAXBSource source = new JAXBSource(ctx, objFac.createDocument(document));
            Schema schema = getSchema();

            Validator validator = schema.newValidator();
            validator.setErrorHandler(new MyErrorHandler());
            validator.validate(source);
        } catch (JAXBException | SAXException | IOException e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

    @Test
    public void validateXml() {
        System.out.println("\n\n*** validateXml   **************");
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                + "<Document xmlns=\"urn:iso:std:iso:20022:tech:xsd:semt.003.001.04\">\n"
                + "    <SctiesBalAcctgRpt/>\n"
                + "</Document>";

        try {
            SchemaFactory sf = SchemaFactory.newInstance(
                    XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = getSchema();
            Validator validator = schema.newValidator();

            // preparing the XML file as a SAX source
            SAXSource source = new SAXSource(
                    new InputSource(new StringReader(xml)));

            // validating the SAX source against the schema
            validator.setErrorHandler(new MyErrorHandler());
            validator.validate(source);
            System.out.println();
            System.out.println("Validation passed.");
        } catch (SAXException | IOException e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

    private Schema getSchema() throws SAXException {
        SchemaFactory sf = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI);
        URL schemaURL = Thread.currentThread().getContextClassLoader().
                getResource(
                "wsdl/Galgo_Posicao_de_Ativos_XSD_Envio_e_Cancelamento_semt.003.001.04.xsd");
        Schema schema = sf.newSchema(schemaURL);
        return schema;
    }

    private static class MyErrorHandler extends DefaultHandler {

        @Override
        public void warning(SAXParseException e) throws SAXException {
            System.out.println("Warning: ");
            printInfo(e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            System.out.println("Error: ");
            printInfo(e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            System.out.println("Fattal error: ");
            printInfo(e);
        }

        private void printInfo(SAXParseException e) {
            System.out.println("   Public ID: " + e.getPublicId());
            System.out.println("   System ID: " + e.getSystemId());
            System.out.println("   Line number: " + e.getLineNumber());
            System.out.println("   Column number: " + e.getColumnNumber());
            System.out.println("   Message: " + e.getMessage());
        }
    }
}
