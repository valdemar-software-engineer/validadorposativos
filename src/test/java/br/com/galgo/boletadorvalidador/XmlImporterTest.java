/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.io.File;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class XmlImporterTest {

    private static final Logger log = LoggerFactory.getLogger(XmlImporterTest.class);

    @Test
    public void importXml() {
        log.info(TestConstants.__TESTE_INI__);
        File xmlFile = new File("Exemplo CPR_REVISAO 2.xml");
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        Assertions.assertThat(xmlImporter.isGalgoAssBalStmt()).isFalse();
        Document document = (Document) xmlImporter.importXml();
        String rptNb = document.getSctiesBalAcctgRpt().getStmtGnlDtls().getRptNb().getShrt();
        Assertions.assertThat(rptNb).isEqualTo("012");
        log.info(TestConstants.__TESTE_FIM__);
    }

    @Test
    public void importCompleteXml() {
        log.info(TestConstants.__TESTE_INI__);
        File xmlFile = new File("testeVerde.xml");
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        Assertions.assertThat(xmlImporter.isGalgoAssBalStmt()).isTrue();
        GalgoAssBalStmtComplexType root = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
        Document document = root.getBsnsMsg().get(0).getDocument();
        String rptNb = document.getSctiesBalAcctgRpt().getStmtGnlDtls().getRptNb().getShrt();
        Assertions.assertThat(rptNb).isEqualTo("001");
        log.info(TestConstants.__TESTE_FIM__);
    }
}
