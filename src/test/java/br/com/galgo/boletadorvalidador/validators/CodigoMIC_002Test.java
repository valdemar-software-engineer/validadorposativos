/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForSubAccountUtils;
import br.com.galgo.boletadorvalidador.utils.ObjectUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import jodd.bean.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 * @author valdemar.arantes
 */
public class CodigoMIC_002Test {

    private static final Logger log = LoggerFactory.getLogger(CodigoMIC_002Test.class);

    @Test
    public void validateAtivoSWAPWithInvalidMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmAttrbts.plcOfListg.id.mktIdrCd",
                    "XTIR");

            //AggregateBalanceInformation13 a = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            AggregateBalanceInformation13 balForAcct = (AggregateBalanceInformation13) BeanUtil.
                    getProperty(doc, "sctiesBalAcctgRpt.balForAcct[0]");

            //balForAcct.getFinInstrmId().getISIN()
            BeanUtil.setPropertyForced(balForAcct, "finInstrmId.ISIN", "ISIN1111");

            AggregateBalanceInformation13 balForSubAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]", balForSubAcct);
            BalanceForSubAccountUtils balUtils = new BalanceForSubAccountUtils(balForSubAcct);
            balUtils.setCodIdentAtivo("SWAP");

            // Código MIC inválido
            balUtils.setCodMIC("AAAA");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 1);
            Assert.assertTrue(
                    errors.get(0).startsWith("O Código MIC \"AAAA\" não existe na ISO 10383. Verificar ativo nr. 1:"));
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateAtivoSWAPWithValidMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmAttrbts.plcOfListg.id.mktIdrCd",
                    "XTIR");

            //AggregateBalanceInformation13 a = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            AggregateBalanceInformation13 balForAcct = (AggregateBalanceInformation13) BeanUtil.
                    getProperty(doc, "sctiesBalAcctgRpt.balForAcct[0]");

            //balForAcct.getFinInstrmId().getISIN()
            BeanUtil.setPropertyForced(balForAcct, "finInstrmId.ISIN", "ISIN1111");

            AggregateBalanceInformation13 balForSubAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]", balForSubAcct);
            BalanceForSubAccountUtils balUtils = new BalanceForSubAccountUtils(balForSubAcct);
            balUtils.setCodIdentAtivo("SWAP");

            // Código MIC válido
            balUtils.setCodMIC("XALG");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 0);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateAtivoSWAPWithoutMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmAttrbts.plcOfListg.id.mktIdrCd",
                    "XTIR");

            //AggregateBalanceInformation13 a = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            AggregateBalanceInformation13 balForAcct = (AggregateBalanceInformation13) BeanUtil.
                    getProperty(doc, "sctiesBalAcctgRpt.balForAcct[0]");

            //balForAcct.getFinInstrmId().getISIN()
            BeanUtil.setPropertyForced(balForAcct, "finInstrmId.ISIN", "ISIN1111");

            AggregateBalanceInformation13 balForSubAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]", balForSubAcct);
            new BalanceForSubAccountUtils(balForSubAcct).setCodIdentAtivo("SWAP");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 1);
            Assert.assertTrue(errors.get(0).startsWith(
                    "O Código MIC é obrigatório para ativos com tipo ANBIMA \"SWAP\". " + "Verificar ativo nr. 1:"));
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateAtivo_CotaDeFundos_WithoutMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            AggregateBalanceInformation13 balForAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmId.ISIN", "ISIN1111");

            //doc.getSctiesBalAcctgRpt().getSubAcctDtls().get(0).getBalForSubAcct().get(0).getFinInstrmId().getOthrId().get(0).setId("SHAR");
            BeanUtil.setDeclaredPropertyForced(doc,
                    "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0].finInstrmId.othrId[0].id", "SHAR");

            //doc.getSctiesBalAcctgRpt().getSubAcctDtls().get(0).getBalForSubAcct().get(0).getFinInstrmId().getOthrId().get(0).getTp().getPrtry();
            BeanUtil.setDeclaredPropertyForced(doc,
                    "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0].finInstrmId.othrId[0].tp.prtry", "TABELA NIVEL 1º");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 0);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateFundosWithInvalidMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmAttrbts.plcOfListg.id.mktIdrCd",
                    "aaaa");

            //AggregateBalanceInformation13 a = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            AggregateBalanceInformation13 balForAcct = (AggregateBalanceInformation13) BeanUtil.
                    getProperty(doc, "sctiesBalAcctgRpt.balForAcct[0]");

            //balForAcct.getFinInstrmId().getISIN()
            BeanUtil.setPropertyForced(balForAcct, "finInstrmId.ISIN", "ISIN1111");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 1);
            Assert.assertTrue(errors.get(0).startsWith(
                    "Código aaaa não existe na ISO 10383. " + "Verificar ativo de código ISIN igual a ISIN1111"));
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateFundosWithValidMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            String codMIC = "BACE";
            Document doc = new Document();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmAttrbts.plcOfListg.id.mktIdrCd",
                    codMIC);

            //AggregateBalanceInformation13 a = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            AggregateBalanceInformation13 balForAcct = (AggregateBalanceInformation13) BeanUtil.
                    getProperty(doc, "sctiesBalAcctgRpt.balForAcct[0]");

            //balForAcct.getFinInstrmId().getISIN()
            BeanUtil.setPropertyForced(balForAcct, "finInstrmId.ISIN", "ISIN1111");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 0);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void validateFundosWithoutMICTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            AggregateBalanceInformation13 balForAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc, "sctiesBalAcctgRpt.balForAcct[0].finInstrmId.ISIN", "ISIN1111");

            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            List<String> errors = new CodigoMIC_002().validate(doc);
            log.debug("errors=\n{}", ObjectUtils.getAsJsonString(errors, true));

            Assert.assertEquals(errors.size(), 0);
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void verificaIso10383FalseTest() {
        log.debug("************************ Início *****************************************");
        try {
            String cod = "AAAA";
            Assert.assertFalse(new CodigoMIC_002().verificaIso10383(cod));
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void verificaIso10383TrueTest() {
        log.debug("************************ Início *****************************************");
        try {
            String cod = "XTIR";
            Assert.assertTrue(new CodigoMIC_002().verificaIso10383(cod));
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

}
