/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author valdemar.arantes
 */
public class ValidatorTest {

    private static final Logger log = LoggerFactory.getLogger(ValidatorTest.class);
    private static final File xmlFile = new File("Exemplo CPR_REVISAO 2.xml");
    private static final File xmlFile_3_schema_errors = new File("Exemplo CPR_REVISAO 2 - 3 ERROS de Schema.xml");

    /*public void validateXml() {
        log.debug("******* Teste: validateXml  *****");
        List<String> errors = Validator.validate(xmlFile, false);
        log.info("Encontrou erros na validação? {}", !errors.isEmpty());
        if (!errors.isEmpty()) {
            printErrors(errors);
        }
        Assert.assertTrue(errors.isEmpty());
        log.debug("******* Teste: Fim de validateXml  *****");
    }*/

    @Test(dataProvider = "validate")
    public void validate(String fileName) {
        log.debug("****************************************\nfileName={}", fileName);
        File xmlFile = new File(fileName);
        Assert.assertTrue(xmlFile.exists() && xmlFile.isFile(), String.format("O arquivo %s não foi encontrado.",
                xmlFile.getAbsolutePath()));
        Assertions.assertThat(xmlFile).isFile();
        List<String> errors = new SchemaValidator().validate(xmlFile);
        printErrors(errors);
        Assertions.assertThat(errors).isEmpty();
    }

    @DataProvider(name = "validate")
    public Object[][] validateDataProvider() {
        return new Object[][]{
                //                {"Erros\\Senior Solution\\2014-09-16\\ArqComNovasTags.xml"},
                //                {"Erros\\Senior Solution\\2014-09-16\\ArqComNovasTags_novoXSD-de-Dados-Suplementares.xml"},
                {"Erros/teste/testeVerde_1_ativo.xml"}

        };
    }

    /*public void testeVerde() {
        log.debug("******* Teste  *****");
        String xmlFilename = "testeVerde.xml";
        File xmlFile = new File(xmlFilename);
        Assert.assertTrue(xmlFile.isFile(), "Arquivo " + xmlFilename + " não encontrado");
        try {
            List<String> errors = Validator.validate(xmlFile, false);
            boolean hasErrors = !errors.isEmpty();
            log.info("Encontrou erros na validação? {}", hasErrors);
            if (hasErrors) {
                printErrors(errors);
            }
            Assert.assertTrue(errors.isEmpty());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }
        log.debug("******* Teste  *****");
    }

    public void testeVerde_1_ativo() {
        log.debug("******* Teste  *****");
        File xmlFile = new File("testeVerde_1_ativo.xml");
        try {
            List<String> errors = Validator.validate(xmlFile, false);
            boolean hasErrors = !errors.isEmpty();
            log.info("Encontrou erros na validação? {}", hasErrors);
            if (hasErrors) {
                printErrors(errors);
            }
            Assert.assertTrue(errors.isEmpty());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        }
        log.debug("******* Teste  *****");
    }

    public void validateTestes() {
        log.debug("******* Teste: ValidacoesRegras_testeJoy  *****");
        try {
            File xmlFile_testes = new File("ValidacoesRegras_TestesJoy.xml");
            Assert.assertTrue(xmlFile.exists() && xmlFile.isFile());
            List<String> errors = Validator.validate(xmlFile_testes, true);
            log.info("Encontrou erros na validação do CPR_REVISAO _TesteJoy? {}", !errors.isEmpty());
            printErrors(errors);
            Assert.assertTrue(errors.isEmpty());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        } finally {
            log.debug("******* Teste: Fim ValidacoesRegras_TesteJoy  *****");
        }

    }

    public void validateRegras() {
        log.debug("******* Teste: ArquivoExemplo  *****");
        File xmlFile_testes = new File("ArquivoExemplo.xml");
        List<String> errors = Validator.validate(xmlFile_testes, true);
        log.info("Encontrou erros na validação do ArquivoExemplo? {}", !errors.isEmpty());
        printErrors(errors);

        Assert.assertTrue(errors.isEmpty());
        log.debug("******* Teste: Fim ArquivoExemplo  *****");

    }*/

    @Test
    public void validateXml_3_Schema_Errors() throws IOException {
        log.debug("******* Teste: validateXml_3_Schema_Errors  *****");
        log.info("Arquivo XML: {}", xmlFile_3_schema_errors.getCanonicalPath());
        List<String> errors = Validator.validate(xmlFile_3_schema_errors, false);
        log.info("Encontrou erros na validação? {}", !errors.isEmpty());
        printErrors(errors);
        Assert.assertEquals(errors.size(), 4);
        log.debug("******* Teste: Fim de validateXml_3_Schema_Errors  *****");
    }

    @Test
    public void xsd_testeVerde() {
        log.debug("******* Teste  *****");
        File xmlFile = new File("Erros/teste/testeVerde_1_ativo.xml");
        try {
            List<String> errors = new SchemaValidator().validate(xmlFile);
            boolean hasErrors = !errors.isEmpty();
            log.info("Encontrou erros na validação? {}", hasErrors);
            if (hasErrors) {
                printErrors(errors);
            }
            Assert.assertTrue(errors.isEmpty());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }
        log.debug("******* Teste  *****");
    }

    /**
     * Loga a lista de erros encontrados na lista errors
     * <p/>
     *
     * @param errors
     */
    private void printErrors(List<String> errors) {
        if (errors == null || errors.isEmpty()) {
            log.debug("Nenhum erro encontrado.");
            return;
        }
        StringBuilder buff = new StringBuilder();
        int counter = 0;
        for (String err : errors) {
            counter++;
            buff.append("\n").append(counter).append(" - ").append(err);
        }
        log.debug(buff.toString());
    }
}
