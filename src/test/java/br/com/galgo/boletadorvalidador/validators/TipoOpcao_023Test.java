package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

/**
 * Created by valdemar.arantes on 18/09/2015.
 */
public class TipoOpcao_023Test {

    private static final Logger log = LoggerFactory.getLogger(TipoOpcao_023Test.class);
    private static Document document;

    @BeforeClass
    public void setUp() throws Exception {
        final File xmlFile = new File("Erros/teste/testeVerde_1_ativo.xml");
        log.info("Abrindo arquivo de Pos. de Ativos {}", xmlFile.getCanonicalPath());
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
        List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
        document = bsnsMsgList.get(0).getDocument();
    }

    @Test
    public void testValidate() throws Exception {
        log.info("********************  Início do Teste  ******************");
        List<String> errors = new TipoOpcao_023().validate(document);
        if (!errors.isEmpty()) {
            int counter = 1;
            for (String error : errors) {
                log.debug(counter + ": " + error);
                counter++;
            }
        }

        Assertions.assertThat(errors).isEmpty();
    }
}