/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.File;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class DespesasLiquidas_029Test {

    private static final Logger log = LoggerFactory.getLogger(DespesasLiquidas_029Test.class);
    private static final File xmlFile = new File(
            "Erros\\Senior Solution\\2014-09-04"
            + "\\FD00000014742195_20111007_20140828164922Envio - Copy.xml");

    @Test
    public void validate() {
        try {
            XmlImporter xmlImporter = new XmlImporter(xmlFile);
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                    importXml();
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            List<String> errors = new DespesasLiquidas_029().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }
}
