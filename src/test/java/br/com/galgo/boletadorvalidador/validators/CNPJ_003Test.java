package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author valdemar.arantes
 */
public class CNPJ_003Test {

    private static final Logger log = LoggerFactory.getLogger(CNPJ_003Test.class);
    private static Document document;

    @BeforeClass
    public static void setupTest() throws IOException {
        final File xmlFile = new File("Erros/teste/testeVerde_1_ativo.xml");
        log.info("Abrindo arquivo de Pos. de Ativos {}", xmlFile.getCanonicalPath());
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
        List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
        document = bsnsMsgList.get(0).getDocument();
    }

    @Test
    public void validateTest() {
        try {
            List<String> errors = new CNPJ_003().validate(document);
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }

            Assertions.assertThat(errors).isEmpty();
/*
            Assertions.assertThat(errors.size()).isEqualTo(1);
            Assertions.assertThat(errors.get(0)).contains(
                    "Não foi encontrada uma identificação de ativo com tipo \"TABELA NIVEL 1º\"");
*/
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

}
