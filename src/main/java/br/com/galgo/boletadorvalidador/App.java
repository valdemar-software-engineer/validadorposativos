package br.com.galgo.boletadorvalidador;

import br.com.galgo.boletadorvalidador.utils.Config;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        System.out.println(App.class.getResource("/log4j.properties").toString());
    }
}
