/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Deve informar código PARV no ConversionPrice para os tipos:
 *
 * Compromissadas - para operações pré-fixadas;
 * Títulos Publicos - para títulos pré-fixados;
 * Títulos Privados - para títulos pré-fixados;
 * Debentures - Conversion Price;
 * Termo com lastro em ativos de renda variável - Conversion Price;
 * Termo com lastro em Ativos em Renda Fixa - Conversion Price;
 *
 * @author joyce.oliveira
 */
@ValidatorType
class PUVencimento_028 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PUVencimento_028.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste PU de Vencimento");

        errors = Lists.newArrayList();

        String tipoAtivo = null;
        String codigoTipoCVM = null;
        boolean verificaPUVencimento = false;
        String tipoPreco = null;
        List<String> listaCodigosCVM = new ArrayList<>();

        //Códigos CVM dos tipos de ativos que devem informar PU da Compra
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            verificaPUVencimento = false;

            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                /**
                 * Conferir se tipo obriga informar PU de Vencimento
                 */
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaPUVencimento = false;

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar se informou OFFR
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaPUVencimento = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                //log.debug("Varialvel Verifica PU de Compra = {}", verificaPUVencimento);
                if (verificaPUVencimento) {
                    // log.debug("Tem que verificar PU de compra pois é do "
                    //        + "tipo {}", codigoTipoCVM);
                    try {
                        tipoPreco = listaAtivos.getFinInstrmAttrbts().getConvsPric().getTp().
                                getValTp().toString();

                        if (!tipoPreco.equals("PARV")) {
                            errors.add("Obrigatório informar código PARV, "
                                    + "referente a PU do Vencimento, quando tipo do ativo "
                                    + "igual a " + codigoTipoCVM + " na Tabela CVM CDA 3.0 B. "
                                    + "Verificar ativo de código " +id[1]+ " igual a " +id[0]+". "
                                    + "<BsnsMsg><SctiesBalAcctgRpt><BalForSubAcct>"
                                    + "<SubAcctDtls><FinInstrmAttrbts><ConvsPric>");
                        }
                    } catch (NullPointerException e) {

                        errors.add("Obrigatório informar código PARV, "
                                + "referente a PU do Vencimento, quando tipo do ativo "
                                + "igual a " + codigoTipoCVM + " na Tabela CVM CDA 3.0 B. "
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0] + ". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><BalForSubAcct>"
                                + "<SubAcctDtls><FinInstrmAttrbts><ConvsPric>");
                    }
                }
            }
        }
        return errors;
    }
}