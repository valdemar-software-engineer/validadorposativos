/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForSubAccountUtils;
import br.com.galgo.boletadorvalidador.utils.Config;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Subseção FinancialInstrumentIdentification:
 * <p/>
 * O campo identificação <Id> deverá ser preenchido somente com números.
 * <p/>
 * <p>
 * <i>(Desde 13/08/2014 - Valdemar)</i><br>
 * <strike>O campo Proprietário &lt;Prtry&gt; deverá ser preenchido com CVM CDA 3.0 Tabela B, deverá
 * ser obrigatoriamente utilizada para identificar o tipo de ativo.</strike>
 * <br/>
 * Manual de Posição de Ativos - Capítulo <h3>8.4.1.2 OUTRA IDENTIFICAÇÃO</h3>
 * <b>Detalhes de:</b> Ativos / Informações sobre os Ativos/ Identificação do Ativo
 * (SubAccountDetails)/ (BalanceForSubAccount) /(FinancialInstrumentIdentification)
 * <br/>
 * <b>Campo:</b> Outra Identificação
 * <br/>
 * Deverá ser obrigatoriamente preenchido o campo de identificação proprietário de acordo com a
 * tabela de identificação de ativos.
 * <br/>
 * <ul>
 * <li> 1º Nível: <b>EQUI OPTN FWRD GOVE CASH FUTU LOAN REPO SWAP DEBE CORP SHAR REAL STRU</b></li>
 * <li> 2º Nível: <b>DERI FLEX CURR EQUI GOVE CORP DEBE</b></li>
 * <li> 3º Nível: <b>REAM RERA</b></li>
 * </ul>
 * <br/>
 * Além disso, convencionou-se que nesta subseção poderá ser indicada qualquer identificação
 * necessária para identificar o ativo tais como CNPJ e código do ativo, conforme cadastro no
 * ambiente de negociação e outros.
 * </p>
 *
 * @author joyce.oliveira
 */
@ValidatorType
class TipoAtivo_005 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(TipoAtivo_005.class);
    private final static String TABELA_IDENTIFIC_ATIVOS = "tabela_identicacao_ativos";
    private final static String ATIVOS_IDENTIFIC_NIVEL_1 = "ativos_identificacao_nivel_1";
    private final static String ATIVOS_IDENTIFIC_NIVEL_2 = "ativos_identificacao_nivel_2";
    private final static String ATIVOS_IDENTIFIC_NIVEL_3 = "ativos_identificacao_nivel_3";
    private final static String prtryIdentificacaoAtivos;
    // Lista com as listas de códigos dos 3 níveis de tipos ANBIMA
    private static final List<List<String>> tipos;

    static {
        prtryIdentificacaoAtivos = Config.getString(TABELA_IDENTIFIC_ATIVOS);
        if (StringUtils.isBlank(prtryIdentificacaoAtivos)) {
            throw new ValidatorException(String.format(
                    " Propriedade %1s não foi encontrada no arquivo de configuração do aplicativo",
                    TABELA_IDENTIFIC_ATIVOS));
        }

        tipos = new ArrayList<>();
        tipos.add(initializeANBIMACodeList(ATIVOS_IDENTIFIC_NIVEL_1));
        tipos.add(initializeANBIMACodeList(ATIVOS_IDENTIFIC_NIVEL_2));
        tipos.add(initializeANBIMACodeList(ATIVOS_IDENTIFIC_NIVEL_3));
    }

    private List<String> errors = Lists.newArrayList();
    private List<OtherIdentification1> othrIdList;

    /**
     * Lê uma lista de strings definida no arquivo config.properties no CLASSPATH da aplicação
     *
     * @param propName <p/>
     * @return
     */
    private static List<String> initializeANBIMACodeList(String propName) {
        String[] arrStr = Config.getStringList(propName);
        if (arrStr == null) {
            throw new ValidatorException(String.format(
                    "Propriedade %1s não foi encontrada no arquivo de configuração do aplicativo", propName));
        } else {
            if (arrStr.length == 0) {
                throw new ValidatorException(String.format("Propriedade %1s não está definida corretamente", propName));
            }
        }
        return Lists.newArrayList(arrStr);
    }

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste de Tipo do Ativo");

        /**
         * Classificação do Tipo do Ativo Será utilizada para identificar o ativo através da tag
         * Classificação Alternativa <AltrnClssfctn> dentro da subseção Classificação do Ativo
         *
         */
        List<SubAccountIdentification16> subAcctDtls = doc.getSctiesBalAcctgRpt().getSubAcctDtls();
        List<AggregateBalanceInformation13> balForSubAcctList;
        SecurityIdentification14 finInstrmId;

        for (SubAccountIdentification16 ativo : subAcctDtls) {
            balForSubAcctList = ativo.getBalForSubAcct();
            //boolean isIdentificProprietErrMsgCreated = false;

            int counter = 0;
            for (AggregateBalanceInformation13 balForSubAcct : balForSubAcctList) {
                counter++;
                try {
                    BalanceForSubAccountUtils balUtils = new BalanceForSubAccountUtils(balForSubAcct);

                    //Capturando a identificação do ativo
                    finInstrmId = balForSubAcct.getFinInstrmId();

                    /**
                     * Verificar Subseção FinancialInstrumentIdentification:
                     */
                    othrIdList = balForSubAcct.getFinInstrmId().getOthrId();
                    int i = -1;
                    boolean isPrtryIdentificacaoAtivosDefined = false;
                    for (OtherIdentification1 othrId : othrIdList) {
                        String othrId_Id = othrId.getId();
                        String othrId_Tp_Prtry = othrId.getTp().getPrtry();
                        String othrId_Tp_Cd = othrId.getTp().getCd();

                        // Não é um ID proprietário, ir para o próximo othrId
                        if (StringUtils.isNotBlank(othrId_Tp_Cd)) {
                            continue;
                        }

                        /*
                         * Não encontrou Tp/Cd, logo, suponho que esteja definido Tp/Prtry,
                         * que é um identificador definido pela ANBIMA.
                         * Verificando se o código está entre os listados pela ANBIMA.
                         */
                        if (StringUtils.equals(othrId_Tp_Prtry, prtryIdentificacaoAtivos)) {
                            isPrtryIdentificacaoAtivosDefined = true;
                            i++;
                            if (!tipos.get(i).contains(othrId_Id)) {
                                String err = String.format("Código %s não existe segundo a tabela de Tipos de Ativo "
                                                + "ANBIMA - Nível %s. Verificar ativo nr. %s: %s", othrId_Id, (i + 1),
                                        counter, balUtils.getIdAsString());
                                log.debug("Inclusão de erro: {}", err);
                                errors.add(err);
                                //isIdentificProprietErrMsgCreated = true;
                            }
                        }
                    }

                    /**
                     * Nenhuma das identificações definidas em OthrId possuíam o tipo com o nome
                     * definido pela ANBIMA.
                     */
                    if (!isPrtryIdentificacaoAtivosDefined) {
                        String err = String.format(
                                "Não foi encontrada uma identificação de ativo com tipo " + "\"%s\". "
                                        + " Verificar ativo nr. %s: %s", prtryIdentificacaoAtivos, counter,
                                balUtils.getIdAsString());
                        log.debug("Inclusão de erro: {}", err);
                        errors.add(err);
                        //isIdentificProprietErrMsgCreated = true;
                    }

                } catch (Exception e) {
                    log.error(null, e);
                    throw new ValidatorException("Erro no validador: " + e.toString());
                }
            }
        }
        return errors;
    }
}
