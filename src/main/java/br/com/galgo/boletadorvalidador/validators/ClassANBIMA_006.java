/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Subseção FinancialInstrumentIdentification:
 * O campo identificação <Id> deverá ser preenchido somente com números.
 * O campo Proprietário <Prtry> deverá ser preenchido com 'Código ANBIMA'
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ClassANBIMA_006 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            ClassANBIMA_006.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Classificação ANBIMA");

        errors = Lists.newArrayList();
        /**
         * Na Sessão <BalanceForAccount>/<FinancialInstrumentIdentification> Se houver Identificação
         * Proprietária igual a 'Código ANBIMA', verificar: O campo identificação <Id> deverá ser
         * preenchido somente com números.
         */
        //TODO como tratar acentos?
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();
        String classANBIMAFundo;
        String idClassANBIMAFundo = null;

        for (AggregateBalanceInformation13 listaFundos : balForAcctList) {
            List<OtherIdentification1> othrId = listaFundos.getFinInstrmId().getOthrId();

            //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaFundos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

            for (OtherIdentification1 listIdentification : othrId) {
                try {
                    classANBIMAFundo = listIdentification.getTp().getPrtry();
                    idClassANBIMAFundo = listIdentification.getId();
                    classANBIMAFundo = StringUtils.trimToEmpty(classANBIMAFundo);
                    if (classANBIMAFundo.equalsIgnoreCase("Codigo ANBIMA")) {
                        if (!StringUtils.isNumeric(idClassANBIMAFundo)) {
                            errors.
                                    add("Identificação Código ANBIMA deve ser preenchida apenas "
                                    + "com dígitos. Foi informado o valor "+idClassANBIMAFundo
                                    + ". Verificar ativo de código "+id[1]+" igual a "+ id[0]
                                    + ". <BsnsMsg><SctiesBalAcctgRpt><BalForAcct><FinInstrmId><OthrId>");
                        }
                    }

                } catch (NullPointerException e) {
                    log.debug("Fundo {} não tem Classificação ANBIMA.", idClassANBIMAFundo);
                }
            }
        }

        return errors;
    }

    void verificaDigitos() {
    }
}
