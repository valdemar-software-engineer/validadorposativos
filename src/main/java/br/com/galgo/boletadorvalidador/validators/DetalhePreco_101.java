/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
@GalgoValidatorType
class DetalhePreco_101 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            DetalhePreco_101.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Detalhe de Preço Duplicado");

        errors = Lists.newArrayList();

        /**
         * Verificar Código MIC do Fundo, se existir
         */
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 listaFundo : balForAcctList) {
            List<PriceInformation5> listaPreco = listaFundo.getPricDtls();

            log.debug("Quantidade de seções PriceDetails : {}", listaPreco.size());
            if(listaPreco.size()>1){
                errors.add("Validação do Sistema Galgo: PA.0453 - Detalhe do Preço Inválido. "
                        + "Verificar seção PriceDetails do Fundo."
                        + "<SctiesBalAcctgRpt><BalForAcct><PricDtls>");
            }
        }


        return errors;
}
}