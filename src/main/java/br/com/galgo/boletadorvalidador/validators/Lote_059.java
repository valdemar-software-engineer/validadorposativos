/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.QuantityBreakdown4;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Lote_059 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Lote_059.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Lote");

        errors = Lists.newArrayList();

        //É obrigatório informar o Lote, quando tipo do Ativo igual a Ações
        List<String> listaCodigosCVM = new ArrayList<>();
        listaCodigosCVM.add("37");

        boolean verificaLote;
        String tipoAtivo;
        String codigoTipoCVM = null;
        List<QuantityBreakdown4> lote;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaLote = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaLote = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                //Se variavel verificaLote igual a true, deve conferir se seção foi informada
                if (verificaLote) {
                    try {
                        lote = listaAtivos.getQtyBrkdwn();

                        if (lote.isEmpty()) {
                            errors.add("Para ativo de tipo CVM igual a " + codigoTipoCVM
                                    + " é obrigatório informar Lote na Seção QuantityBreakdown. "
                                    + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                    + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                    + "<BalForSubAcct><QtyBrkdwn>");
                        }
                    } catch (NullPointerException e) {
                        errors.add("Para ativo de tipo CVM igual a " + codigoTipoCVM
                                + " é obrigatório informar Lote na Seção QuantityBreakdown. "
                                + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><QtyBrkdwn>");
                    }
                }
            }
        }
        return errors;
    }
}
