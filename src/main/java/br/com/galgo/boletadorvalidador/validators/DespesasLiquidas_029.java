/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author joyce.oliveira
 *         <p/>
 *         Valor Financeiro Total das Despesas = Somatória das Despesas Informadas na subsseção
 *         AdditionalBalanceBreakdownDetails
 */
@ValidatorType
class DespesasLiquidas_029 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(DespesasLiquidas_029.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Despesas Liquidas");

        errors = Lists.newArrayList();

        SecuritiesBalanceAccountingReportV04 mensagem = doc.getSctiesBalAcctgRpt();
        String codigoTipo = null;
        String emissor = null;
        String identificacao = null;

        List<AggregateBalanceInformation13> balForAcctLst = mensagem.getBalForAcct();

        for (AggregateBalanceInformation13 balForAcct : balForAcctLst) {
            List<SubBalanceInformation6> balBrkdwnLst = balForAcct.getBalBrkdwn();

            //Capturando a identificação do ativo
            Identificador identifica = new Identificador();
            SecurityIdentification14 listaIdentificadoresAtivo = balForAcct.getFinInstrmId();
            String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

            for (SubBalanceInformation6 listaPatrimonioFundo : balBrkdwnLst) {
                BigDecimal valorTotalDespesas = null;
                BigDecimal valorDespesa = null;
                BigDecimal valorTotalDespesasArquivo = null;
                valorTotalDespesas = BigDecimal.ZERO;
                try {
                    codigoTipo = listaPatrimonioFundo.getSubBalTp().getPrtry().getId();
                    //log.debug("Codigo igual a {}", codigoTipo);
                    if (codigoTipo.equals("EXPN")) {
                        //Emissor deve ser igual ao valor da propriedade ISO_20022
                        emissor = listaPatrimonioFundo.getSubBalTp().getPrtry().getIssr();
                        if (!emissor.equals(Config.getString("ISO_20022"))) {
                            errors.add("Para Despesas Liquidas do fundo, identificada pelo código"
                                    + " EXPN, deve ser informado o emissor " + Config.getString("ISO_20022") + ". Foi "
                                    + "informado " + emissor + ". " + "Verificar ativo de código " + id[1] + " igual a "
                                    + id[0] + ". " + "<BsnsMsg><SctiesBalAcctgRpt<BalForAcct>"
                                    + "<BalBrkdwn><SubBalTp><Prtry><Issr>");
                        }
                        //Identificação deve ser igual a Expenses
                        identificacao = listaPatrimonioFundo.getSubBalTp().getPrtry().getSchmeNm();
                        if (!identificacao.equals("Expenses")) {
                            errors.add("Para Despesas Liquidas do fundo, identificada pelo código"
                                    + " EXPN, deve ser informada a identificação Expenses, " + "foi informado "
                                    + identificacao + ". " + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                    + ". " + "<BsnsMsg><SctiesBalAcctgRpt<BalForAcct>"
                                    + "<BalBrkdwn><SubBalTp><Prtry><SchmeNm>");
                        }
                        //Identificando o Valor Financeiro Total de Despesas
                        valorTotalDespesasArquivo = listaPatrimonioFundo.getQty().getQty().
                                getFaceAmt();
                        //log.debug("Valor Despesa informado no arquivo : {}", valorTotalDespesasArquivo);
                        List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = listaPatrimonioFundo.
                                getAddtlBalBrkdwnDtls();

                        for (AdditionalBalanceInformation6 listaInfAdd : addtlBalBrkdwnDtlsLst) {
                            valorDespesa = listaInfAdd.getQty().getQty().getFaceAmt();
                            //log.debug("Valor despesa = {}", valorDespesa);
                            valorTotalDespesas = valorTotalDespesas.add(valorDespesa);
                            //log.debug("Valor total despesa = {}", valorTotalDespesas);
                        }
                    }
                } catch (NullPointerException e) {
                }
                //Confere se Valor Informado no arquivo é igual a somatória dos valores de despesa informados
                try {
                    log.debug("[{}={}] valorTotalDespesasArquivo={}; valorTotalDespesas={}", id[1], id[0],
                            valorTotalDespesasArquivo, valorTotalDespesas);
                    if (valorTotalDespesasArquivo.compareTo(valorTotalDespesas) != 0) {
                        String errMsg = "Valor Financeiro Total de Despesas,"
                                + "identificada pelo código EXPN, deve ser igual a somatória das "
                                + "despesas informadas. Foi informado o Valor Financeiro Total de "
                                + "despesas igual a " + valorTotalDespesasArquivo + ". " + "Verificar ativo de código "
                                + id[1] + " igual a " + id[0] + ". " + "<BsnsMsg><SctiesBalAcctgRpt><BalForAcct>"
                                + "<BalBrkdwn><SubBalTp><Prtry>";
                        log.debug(errMsg);
                        errors.add(errMsg);
                    }
                } catch (NullPointerException e) {
                }
            }
        }
        return errors;
    }
}
