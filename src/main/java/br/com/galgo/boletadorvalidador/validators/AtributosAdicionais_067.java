/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.DerivativeBasicAttributes1;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Para o tipo de ativo Foward de Moeda, deve informar o campo
 * Atributos Adicionais - Notional (Additional Derivative Attributes)
 *
 * @author joyce.oliveira
 */
@ValidatorType
class AtributosAdicionais_067 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            AtributosAdicionais_067.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Atributos Adicionais");

        errors = Lists.newArrayList();
        boolean verificaAtributosAdicionais;
        String tipoAtivo;
        String codigoTipoCVM = null;
        Identificador identifica = new Identificador();
        //Códigos CVM dos tipos de ativos que devem informar Atributos Adicionais
        List<String> listaCodigosCVM = new ArrayList<>();
        listaCodigosCVM.add("48");
        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Informar Campo Interest Rate
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id;
                id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaAtributosAdicionais = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar se informou OFFR
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaAtributosAdicionais = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                if(verificaAtributosAdicionais){
                    try {
                        DerivativeBasicAttributes1 atributosAdicionais = listaAtivos.getAddtlDerivAttrbts();
                        log.debug("Encontrados atributos adicionais : {}", atributosAdicionais.getNtnlCcyAndAmt().getValue());

                    } catch (NullPointerException e) {
                        errors.add(" Obrigatório informar Atributos Adicionais na subseção "
                                + "AdditionalDerivativeAttributes para ativo de tipo CVM "
                                + "igual a "+codigoTipoCVM+ ". Verificar ativo de código "
                                +id[1]+ " igual a "+id[0]+". <BsnsMsg>"
                                + "<SctiesBalAcctgRpt><SubAcctDtls><BalForSubAcct>"
                                + "<AddtlDerivAttrbts><NtnlCcyAndAmt> ");

                    }
                }
            }
        }
    return errors;
    }
}
