/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 *Obrigatório informar o campo (Taxa de Juros – Interest Rate) para os tipos:
 *
 * Compromissadas
 * Títulos Publicos
 * Títulos Privados
 * Debentures
 * Termo em RF
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Cupom_033 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Cupom_033.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Cupom");

        errors = Lists.newArrayList();
        Identificador identifica = new Identificador();

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Cupom
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        boolean verificaCupom = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Informar Campo Interest Rate
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {

                //Capturando a identificação do ativo
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id;
                id = identifica.retornaIdentificador(listaIdentificadoresAtivo);
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaCupom = false;

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaCupom = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if(verificaCupom){
                    try {
                        BigDecimal taxaJuros = listaAtivos.getFinInstrmAttrbts().getIntrstRate();
                        //log.debug("Valor da Taxa de Juros foi informado :{}", taxaJuros);
                        if(taxaJuros == null){
                          //  log.debug("Obrigatorio informar taxa de juros");
                        }
                    } catch (NullPointerException e) {
                        errors.add("Obrigatório informar Taxa de Juros, na subseção InterestRate,"
                                + " para tipo de ativo de "
                                + "Código CVM igual a "+codigoTipoCVM+ ". Verificar ativo "
                                + "de código " +id[1]+ " igual a "+id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts><IntrstRate>");
                    }
                }
            }
        }
        return errors;
    }
}
