/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *Para opções, é obrigatório informar se estilo é Americana ou Europeia
 *
 * @author joyce.oliveira
 */
@ValidatorType
class EstiloOpcao_024 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            EstiloOpcao_024.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Estilo Opção");

        errors = Lists.newArrayList();

        //Verificando se é tipo que obriga informar o estilo de opção:
        //Opções de Derivativos; Opções de Moedas; Opções Flexíveis

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Estilo de Opção
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");

        boolean verificaEstiloOpcao;
        String tipoAtivo;
        String codigoTipoCVM = null;

        //!! TODO o tipo de Opção de Ações não precisa deste tipo de verificação,
        //como identificar se é Opção de Ação?
        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            //Verificar se tipo do ativo obriga Informar Campo Interest Rate
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaEstiloOpcao = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaEstiloOpcao = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                if (verificaEstiloOpcao) {
                    //Tem que testar quando não informou valor, pois é opcional
                    log.debug("Ativo de codigo CVM {} tem que informar Estilo de Opção!",
                            codigoTipoCVM);
                    try {
                        String estiloOpcao = listaAtivos.getFinInstrmAttrbts().getOptnStyle().
                                getCd().toString();
                        //log.debug("Valor deve ser igual a AMER ou EURO");

                    } catch (NullPointerException e) {
                        errors.add("Para tipo de ativo de código CVM igual a " + codigoTipoCVM
                                + ", deve ser informado o Estilo de Opção, tag <OptnStyle>. "
                                + " Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts><OptnStyle><Cd>");
                    }
                }
            }
        }
        return errors;
    }
}
