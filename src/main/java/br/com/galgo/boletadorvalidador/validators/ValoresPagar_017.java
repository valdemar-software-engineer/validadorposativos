/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * Detalhes da Carteira (BalanceForAccount)/(BalanceBreakdown)
 * <p/>
 * Quando sessão apresentar código PENR, indica valores a receber Deve ser informado na subseção
 * ADDITIONALBALANCEBREAKDOWN DETAILS se ativo é tributado (Classe Impostos verifica se tributado) e
 * valor financeiro referente a quantidade informada como disponível, em garantia, etc...
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValoresPagar_017 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(ValoresPagar_017.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Valores a Pagar");

        errors = Lists.newArrayList();

        /**
         * Verificar Valores a Pagar PAYA
         *
         * Valor Financeiro total a pagar = soma das despesas informadas
         *
         */
        List<AggregateBalanceInformation13> balForAcctList = null;
        //        String identificacao = null;

        balForAcctList = doc.getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 listaFundo : balForAcctList) {
            List<SubBalanceInformation6> balBrkdwnLst = listaFundo.getBalBrkdwn();

            for (SubBalanceInformation6 listaPatrimonioFundo : balBrkdwnLst) {
                //Verificar se é PAYA:
                String codigoTipo = null;
                try {
                    codigoTipo = listaPatrimonioFundo.getSubBalTp().getPrtry().getId();
                    //Se for igual a PAYA, somar os valores
                    if (codigoTipo.equals("PAYA")) {
                        BigDecimal totalDespesas = BigDecimal.ZERO;
                        BigDecimal valorFinTotalPagar = null;

                        //Conferindo se Emissor igual ao valor da propriedade ISO_20022
                        String emissor = listaPatrimonioFundo.getSubBalTp().getPrtry().getIssr();
                        if (!emissor.equalsIgnoreCase(Config.getString("ISO_20022"))) {
                            errors.add("Quando indicar Valores a Pagar, PAYA, é obrigatório "
                                    + "informar o Emissor igual a " + Config.getString("ISO_20022") + ". Foi "
                                    + "informado " + emissor + ". <BalForAcct><BalBrkdwn>" + "<SubBalTp><Prtry><Issr>");
                        }

                        String identificacao = listaPatrimonioFundo.getSubBalTp().getPrtry().getSchmeNm();
                        if (!identificacao.equalsIgnoreCase("Payables")) {
                            errors.add("Quando indicar Valores a Pagar, PAYA, é obrigatório "
                                    + "informar o Identificação igual a 'Payables'. Foi " + "informado " + identificacao
                                    + ". <BalForAcct><BalBrkdwn>" + "<SubBalTp><Prtry><SchmeNm>");
                        }

                        //Verificar qual o valor financeiro total a pagar
                        valorFinTotalPagar = listaPatrimonioFundo.getQty().getQty().getFaceAmt();
                        //log.debug("Valor Financeiro total a pagar = {}", valorFinTotalPagar);
                        List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = listaPatrimonioFundo.
                                getAddtlBalBrkdwnDtls();

                        for (AdditionalBalanceInformation6 listaDetalhes : addtlBalBrkdwnDtlsLst) {
                            BigDecimal despesa = listaDetalhes.getQty().getQty().getFaceAmt();
                            totalDespesas = totalDespesas.add(despesa);
                            //log.debug("Despesa do fundo, igual a: {}", despesa);
                            //log.debug("Total de despesas = {}", totalDespesas);
                        }

                        // conferir se Valor Financeiro Total a Pagar igual a somatória das
                        // despesas informadas:
                        if (valorFinTotalPagar.compareTo(totalDespesas) != 0) {
                            String errMsg = "Valor Financeiro Total a Pagar, identidicado por PAYA, deve ser igual à "
                                    + "soma dos valores a pagar informados. Valor Financeiro"
                                    + " Total a Pagar informado igual a " + valorFinTotalPagar
                                    + ", e somatória das despesas informadas igual a " + totalDespesas
                                    + ". <BalForAcct><BalBrkdwn><Qty><Qty><FaceAmt>";
                            log.debug(errMsg);
                            errors.add(errMsg);
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem id, verifica o próximo");
                }
            }


            //            try {
            //                if (valorFinTotalPagar.compareTo(totalDespesas) != 0) {
            //                    //log.debug("Valor Financeiro não confere");
            //                    errors.
            //                            add("Valor Financeiro Total a Pagar, identidicado por PAYA, deve ser igual à "
            //                                    + "soma dos valores a pagar informados. Valor Financeiro"
            //                                    + " Total a Pagar informado igual a " + valorFinTotalPagar
            //                                    + ", e somatória das despesas informadas igual a "
            //                                    + totalDespesas + ". <BalForAcct><BalBrkdwn><Qty><Qty><FaceAmt>");
            //                }
            //            } catch (NullPointerException e) {
            //                errors.add(
            //                        "Valor Financeiro Total a Pagar, identidicado por PAYA, deve ser igual à "
            //                        + "soma dos valores a pagar informados. Valor Financeiro"
            //                        + " Total a Pagar informado igual a " + valorFinTotalPagar
            //                        + ", e somatória das despesas informadas igual a "
            //                        + totalDespesas + ". "
            //                        + "<BalForAcct><BalBrkdwn><SubBalTp><Prtry><Id>");
            //            }
            //        }
        }
        return errors;
    }
}
