/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceBreakdownUtils;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.galgo.utils.PropertyUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author joyce.oliveira
 */
//TODO Validação removida. Precisa ser totalmente reescrita.
//@ValidatorType
class CotasResgatar_014 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CotasResgatar_014.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Cotas a Resgatar");

        errors = Lists.newArrayList();

        /**
         * Verificar Cotas a Emitir
         *
         * Valor financeiro = Quantiade de cotas * Valor da Cota
         */
        BigDecimal qntdCotas = BigDecimal.ZERO;
        BigDecimal valorFinanceiro = BigDecimal.ZERO;
        BigDecimal valorCota = BigDecimal.ZERO;
        BigDecimal totalCotas = BigDecimal.ZERO;
        List<AggregateBalanceInformation13> balForAcctList = null;
        try {
            balForAcctList = doc.getSctiesBalAcctgRpt().getBalForAcct();
        } catch (NullPointerException e) {
            log.error("NPE ao executar doc.getSctiesBalAcctgRpt().getBalForAcct(). Retornando.");
            return errors;
        }

        for (AggregateBalanceInformation13 fundo : balForAcctList) {
            List<SubBalanceInformation6> balBrkdwnLst = fundo.getBalBrkdwn();

            valorFinanceiro = getValorFinanceiro(fundo);
            log.debug("valorFinanceiro={}", valorFinanceiro);

            for (SubBalanceInformation6 balBrkdwn : balBrkdwnLst) {
                try {
                    BalanceBreakdownUtils balBrkdwnUtils = new BalanceBreakdownUtils(balBrkdwn, fundo);

                    SecuritiesBalanceType12Code codigoTipo = balBrkdwnUtils.getCodigoTipo();
/*
                    try {
                        codigoTipo = balBrkdwn.getSubBalTp().getCd();
                    } catch (NullPointerException e) {
                        continue;
                    }
*/

                    //log.debug("Tipo código preço = {}", codigoTipo);
                    //Se for igual a PENR, somar os valores
                    if (SecuritiesBalanceType12Code.PENR.equals(codigoTipo)) {

                        //Verificar quantidade de cotas
                        qntdCotas = (BigDecimal) getValue(balBrkdwn, "qty.qty.unit", BigDecimal.ZERO);
                        //qntdCotas = balBrkdwn.getQty().getQty().getUnit();

                        //Verificar qual o valor da cota nas Informações Adicionais
                        List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = balBrkdwn.getAddtlBalBrkdwnDtls();
                        for (AdditionalBalanceInformation6 addtlBalBrkdwnDtls : addtlBalBrkdwnDtlsLst) {
                            SecuritiesBalanceType7Code subBalTp_Cd = addtlBalBrkdwnDtls.getSubBalTp().getCd();
                            if (SecuritiesBalanceType7Code.DIRT.equals(subBalTp_Cd)
                                    || SecuritiesBalanceType7Code.CLEN.equals(subBalTp_Cd)) {
                                valorCota = (BigDecimal) getValue(addtlBalBrkdwnDtls, "qty.qty.faceAmt", BigDecimal.ZERO);
                                break;
                            }
                        }
                        totalCotas.add(valorCota.multiply(qntdCotas));
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>, verifica o próximo");
                }
                //Capturar o valor da Cota
                List<PriceInformation5> pricDtlsLst = fundo.getPricDtls();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = fundo.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                for (PriceInformation5 listaPrecos : pricDtlsLst) {
                    valorCota = listaPrecos.getVal().getAmt().getValue();
                }

                //conferir se Valor financeiro = Quantiade de cotas * Valor da Cota
                try {
                    log.info("valorFinanceiroCotasResgatar={}; valorCota={}; qntdCotas={}",
                            valorFinanceiro, valorCota, qntdCotas);
                    if ((valorFinanceiro == null)
                            && (valorCota == null) && (qntdCotas == null)) {
                        log.info("valorFinanceiroCotasResgatar, valorCota e qntdCotas nulos. "
                                + "Não há validação neste caso.");
                        return errors;
                    }

                    if ((valorFinanceiro == null)
                            || (valorCota == null) || (qntdCotas != null)) {
                        String errMsg = "Valor total de Cotas a Resgatar + ("
                                + valorFinanceiro + "), representado pelo código "
                                + "PENR, ou Valor da Cota (" + valorCota
                                + ") ou a Quantidade de"
                                + "Cotas (" + qntdCotas + ") nulo(s). "
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                + ". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><BalForAcct><BalBrkdwn><SubBalTp><Cd>";
                        log.debug(errMsg);
                        errors.add(errMsg);
                        return errors;
                    }

                    if (valorFinanceiro.compareTo(valorCota.multiply(qntdCotas)) != 0) {
                        errors.add(
                                "Valor total de Cotas a Resgatar, representado pelo código PENR, deve ser igual ao "
                                        + "Valor da Cota multiplicado pela Quantidade de Cotas. "
                                        + "Foi informado Valor de Cotas a Resgatar igual a "
                                        + valorFinanceiro + ", Valor da Cota "
                                        + "igual a " + valorCota + ", e Quantidade de Cotas "
                                        + "igual a " + qntdCotas + ". "
                                        + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                        + ". "
                                        + "<BsnsMsg><SctiesBalAcctgRpt><BalForAcct><BalBrkdwn><SubBalTp><Cd>");
                    }
                } catch (Exception e) {
                    log.error(null, e);
                }
            }
        }
        return errors;
    }

    /**
     * Retorna o valor financeiro de um fundo ou carteira.
     * Se o valor não for encontrado no XML, retorna 0.
     * <p/>
     *
     * @param fundo <p/>
     * @return
     */
    private BigDecimal getValorFinanceiro(AggregateBalanceInformation13 fundo) {

        BigDecimal valorFinanc = null;
        try {
            valorFinanc = fundo.getAcctBaseCcyAmts().getHldgVal().getAmt().getValue();
        } catch (NullPointerException e) {
            log.error(e.toString());
        }
        return valorFinanc == null ? BigDecimal.ZERO : valorFinanc;
    }

    /**
     * Retorna o valor de um objeto a partir da sua propriedade. Se o valor não for
     * encontrado ou for nulo, retorna defaultValue.
     *
     * @param bean
     * @param property
     * @param defaultValue <p/>
     * @return
     */
    private Object getValue(Object bean, String property,
                            Object defaultValue) {
        Object value = PropertyUtils.getPropertyDN(bean, property);
        return value == null ? defaultValue : value;
    }
}
