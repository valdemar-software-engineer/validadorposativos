/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Através do campo OtherIdentification deverá ser identificado se a
 * operação compromissada é de compra ou venda através dos códigos:
 * REAM – Compromisso de compra
 * RERA – Compromisso de Venda
 *
 * No campo Tipo, deverá ser identificada a operação compromissada através do
 * código REPO – operação compromissada.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Compromissadas_041 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Compromissadas_041.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Compromissada");

        errors = Lists.newArrayList();

        String tipoAtivo = null;
        String codigoTipoCVM = null;
        boolean verificaCompromissada = false;
        String idCompromisssada = null;
        String codCompromissada = null;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo igual a compromissada, código 147
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaCompromissada = false;

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();

                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        try {
                            if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                                //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                                codigoTipoCVM = listaIdAtivos.getId();
                                if (codigoTipoCVM.equals("147")) {
                                    verificaCompromissada = true;
                                    break;
                                }
                                break;
                            }
                        } catch (NullPointerException e) {
                            //log.debug("<Prtry> nulo!");
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                //Verifica se foi informado código REPO para <Cd><Tp><OthrId><FinInstrmId>
                //Verificar se foi informado código REAM ou RERA  <Id><OthrId><FinInstrmId>
                if (verificaCompromissada) {
                    boolean existeId = false;
                    boolean existeCod = false;

                    for (OtherIdentification1 listaIdAtivos2 : othrIdLst) {
                        try {
                            idCompromisssada = listaIdAtivos2.getId();

                            if (idCompromisssada.equals("REAM") || idCompromisssada.equals("RERA")) {
                                log.debug("Encontrou tipo {}", idCompromisssada);
                                existeId = true;
                            }
                        } catch (NullPointerException e) {
                            log.debug("Não tem <Id>");
                        }
                        try {
                            codCompromissada = listaIdAtivos2.getTp().getCd();
                            if (codCompromissada.equals("REPO")) {
                                //log.debug("Encontrou código {}", codCompromissada);
                                existeCod = true;
                            }
                        } catch (NullPointerException e) {
                            //log.debug("Não tem <Cd>");
                        }
                    }
                    if(!existeId){
                        errors.add("Obrigatório informar o código REAM, se for Compromissada"
                                + "de Compra, ou código RERA se for Compromissada de Venda"
                                + "para ativo de tipo CVM igual a " +codigoTipoCVM
                                + ". Verificar ativo de código "+id[1]+" igual a "+ id[0]
                                + ". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmId><OthrId><Id>" );
                    }
                    if(!existeCod){
                        errors.add("Obrigatório informar o código REAM, se for Compromissada"
                                + "de Compra, ou código RERA se for Compromissada de Venda"
                                + "para ativo de tipo CVM igual a " +codigoTipoCVM
                                + ". Verificar ativo de código "+id[1]+" igual a "+ id[0]
                                + ". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmId><OthrId><Tp><Cd>" );
                    }
                }
            }
        }
        return errors;
    }
}
