/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import jodd.bean.BeanUtil;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Tipo de Opção (OptionType), tag obrigatória para os seguintes tipos de ativos:
 * <p/>
 * Opções de derivativos;
 * Opções de moedas;
 * Opções flexíveis.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class TipoOpcao_023 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(TipoOpcao_023.class);
    private static final String[] tpAtivoComTpOpcaoObrigatorio = Config.getStringList("tp_ativo_tpopcao_obrigatorio");
    private static final String TABELA_NIVEL_1 = Config.getString("tabela_identicacao_ativos");
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Validando o Tipo de Opção - OptnTp");

        errors = Lists.newArrayList();

        // Códigos CVM dos ativos que devem declarar o Tipo de Oopção

        //Verificando se o ativo é do tipo que obriga informar o tipo de opção:
        //Opções de Derivativos; Opções de Moedas; Opções Flexíveis

        List<String> listaCodigosCVM = Lists.newArrayList("39", "40");
        //Códigos CVM dos tipos de ativos que devem informar Tipo de Opção
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");

        boolean isTipoOpcao;
        String tipoAtivo;
        String othrId_Id = null;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {

            // Recuperando a lista de ativos de um fundo ou carteira
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.getBalForSubAcct();

            //Verificar se tipo do ativo obriga Informar Tipo de Opção
            for (AggregateBalanceInformation13 ativo : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = ativo.getFinInstrmId().getOthrId();

                //Capturando a identificação do ativo
                Identificador finInstrmIdHelper = new Identificador();
                SecurityIdentification14 finInstrmId = ativo.getFinInstrmId();
                String[] id = finInstrmIdHelper.retornaIdentificador(finInstrmId);

                // Um ativo é do tipo Opção quando encontrar pelo menos um OthrId atender às seguintes condições:
                // 1 - OthrId/Tp/Prtry = TABELA NIVEL 1
                // 2 - OthrId/Id = OPTN
                isTipoOpcao = false;
                try {
                    for (OtherIdentification1 othrId : othrIdLst) {
                        tipoAtivo = (String) BeanUtil.getDeclaredPropertySilently(othrId, "tp.prtry");

                        if (TABELA_NIVEL_1.equals(tipoAtivo)) {
                            othrId_Id = othrId.getId();
                            if (ArrayUtils.contains(tpAtivoComTpOpcaoObrigatorio, othrId_Id)) {
                                isTipoOpcao = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                }

                if (isTipoOpcao) {
                    // O Tipo de Opção (OptnTp) e Estilo de Opção (OptnStyle) são obrigatórios neste caso
                    if (BeanUtil.getDeclaredPropertySilently(ativo, "finInstrmAttrbts.optnTp") == null) {
                        String errMsg = String.format("Para tipo de ativo de código %s, deve ser informado o "
                                        + "Tipo de Opção. Verificar ativo de código %s igual a %s. "
                                        + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts><OptnTp><Cd>", othrId_Id,
                                id[1], id[0]);
                        errors.add(errMsg);
                    }
                    final Object optnStyleCd = BeanUtil.getDeclaredPropertySilently(ativo,
                            "finInstrmAttrbts.optnStyle.cd");
                    if (optnStyleCd == null) {
                        String errMsg = String.format("Para tipo de ativo de código %s, deve ser informado o "
                                        + "Estilo de Opção. Verificar ativo de código %s igual a %s. "
                                        + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts><OptnTp><Cd>", othrId_Id,
                                id[1], id[0]);
                        errors.add(errMsg);
                    }
                }
            }
        }

        return errors;
    }
}
