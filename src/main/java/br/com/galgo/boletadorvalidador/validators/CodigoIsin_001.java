package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Código ISIN<br/><br/>
 *
 * ISO 6166 Código de 12 dígitos composto de: Código de dois caracteres (Letras) ISO 3166 + Código
 * Base com nove caracteres (Alfa-numérico) + dígito numérico verificador
 *
 * Se o ativo financeiro for identificado através de código ISIN, o código CFI é obrigatório.
 *
 * @author valdemar.arantes
 */
@ValidatorType
class CodigoIsin_001 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CodigoIsin_001.class);
    private List<String> errors = Lists.newArrayList();
    private static final List<String> iso3166 = Lists.newArrayList(Config.getStringList("codigos_iso3166"));

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste ISIN");

        errors = Lists.newArrayList();

        List<String> listaISIN = Lists.newArrayList();

        //Lista para verifica se Código de dois primeiros caracteres (Letras) pertencem a ISO 3166
        String codISO3166 = null;

        //Codigo ISIN do Fundo
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();
        String isinFundo;
        try {
            isinFundo = balForAcctList.get(0).getFinInstrmId().getISIN();
            codISO3166 = isinFundo.substring(0, 2);

            if (!iso3166.contains(codISO3166)) {
                errors.add("Código " + codISO3166
                        + " não existe na ISO 3166, referente ao Fundo de código ISIN = "
                        + isinFundo + ". <SctiesBalAcctgRpt> <BalForAcct> "
                        + "<FinInstrmId> <ISIN>");
            }

        } catch (NullPointerException e) {
        }

        //Lista para Codigo ISIN de Ativos
        List<SubAccountIdentification16> subAcstDtls = doc.
                getSctiesBalAcctgRpt().getSubAcctDtls();

        for (SubAccountIdentification16 listaInfAtivos : subAcstDtls) {

            List<AggregateBalanceInformation13> balForSubAcctLst = listaInfAtivos.getBalForSubAcct();

            String isinAtivo = null;


            //Se ISIN existir verifica se dois primeiros caracteres pertencem a ISO 3166
            for (AggregateBalanceInformation13 ativo : balForSubAcctLst) {
                if (isinAtivo == null) {
                    continue;
                }

                try {
                    isinAtivo = ativo.getFinInstrmId().getISIN();
                    codISO3166 = isinAtivo.substring(0, 2);
                    if (!iso3166.contains(codISO3166)) {
                        errors.add("Código " + codISO3166
                                + " não existe na ISO 3166, referente ao Ativo de código ISIN = "
                                + isinAtivo + ". <SctiesBalAcctgRpt> <SubAcctDtls> <BalForSubAcct>"
                                + "<FinInstrmId> <ISIN>");

                        //Salvar código ISIN em uma lista, para verificar se existem ativos com
                        //código ISIN repetido
                    } else {
                        listaISIN.add(isinAtivo);
                    }

                    //log.debug("ISIN do Ativo = " + isinAtivo);
                } catch (NullPointerException e) {
                    log.error(null, e);
                }

                //Se Ativo for identificado por ISIN, então deve ter Código CFI
                if (isinAtivo != null) {
                    try {
                        String cfiAtivo = ativo.getFinInstrmAttrbts().getClssfctnTp().
                                getClssfctnFinInstrm();
                        //log.debug("CFI do Ativo = " + cfiAtivo);

                    } catch (Exception e) {
                        String err = "Não existe Código CFI para o Ativo de Código ISIN = "
                                + isinAtivo + ".<SctiesBalAcctgRpt> "
                                + "<SubAcctDtls> <BalForSubAcct> <FinInstrmAttrbts> "
                                + "<ClssfctnTp> <ClssfctnFinInstrm>";
                        log.debug("Inclusão de erro: {}", err);
                        errors.add(err);
                    }
                }
            }
        }

        List<String> temp = new ArrayList<>(listaISIN);
        for (String isinRepetido : listaISIN) {
            temp.remove(isinRepetido);
            if (temp.contains(isinRepetido)) {
                String err = "Código ISIN é unico para cada ativo. O Código ISIN :" + isinRepetido
                        + " foi informado mais de uma vez. <SctiesBalAcctgRpt> "
                        + "<SubAcctDtls>";
                log.debug("Inclusão de erro: {}", err);
                errors.add(err);
            }
        }

        return errors;
    }

    boolean verificaISO3166(String cod) {
        return iso3166.contains(cod);
    }
}
