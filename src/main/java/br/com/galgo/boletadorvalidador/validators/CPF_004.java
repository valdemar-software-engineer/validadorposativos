package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * O campo identificação <Id> deverá ser preenchido somente com números.
 *
 * O campo Nome <SchmeNm> deverá ser preenchido com CNPJ
 *
 * O campo Emissor <Issr> deverá ser preenchido com Receita Federal
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CPF_004 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CPF_004.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste do CPF");

        errors = Lists.newArrayList();

        /**
         * Verificar código CPF na sessão BalanceForAccount
         */
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();
        String cpfFundo;
        String tipoIdFundo = null;

        for (AggregateBalanceInformation13 listaFundos : balForAcctList) {
            List<OtherIdentification1> othrId = null;
            try {
                othrId = listaFundos.getFinInstrmId().getOthrId();
                for (OtherIdentification1 listIdentification : othrId) {
                    cpfFundo = listIdentification.getId();
                    cpfFundo = StringUtils.trimToEmpty(cpfFundo);

                    try {
                        tipoIdFundo = listIdentification.getTp().getCd();
                        if (tipoIdFundo.equals("CPF")) {
                            if (!StringUtils.isNumeric(cpfFundo)) {
                                errors.add("Campo CPF deve conter apenas números. Foi informado "
                                        + cpfFundo +". <BalForAcct><FinInstrmId><OthrId><Tp><Cd>");
                            }
                        }
                    } catch (NullPointerException e) {
                        // log.debug("É identificação Proprietária!");
                    }
                }
            } catch (NullPointerException e) {
                //log.debug("Fundo não tem Tipo codigo", e);
            }
        }
        return errors;
    }
}
