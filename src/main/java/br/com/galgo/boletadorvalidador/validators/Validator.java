/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.reflect.ClassPath;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.io.File;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe utilizada para executar as validações de negócio e Schema da Posição de ativos. Esta é a
 * única classe visível fora deste pacote Java. As validações de negócio não serão realizadas caso
 * existam erros de Schema.
 *
 * @author valdemar.arantes
 */
public class Validator {

    private static final Logger log = LoggerFactory.getLogger(Validator.class);
    private static List<Class<Validator_>> validatorClasses;
    private static List<Class<Validator_>> galgoValidatorClasses;
    private static final String BUS_PCKG = "br.com.galgo.boletadorvalidador.validators";

    private File xmlFile;
    private boolean validarGalgo;
    private ErrorType errorType = null;
    private List<String> errors = null;

    public enum ErrorType {

        XSD, ANBIMA
    }

    static {
        initialize();
    }

    public Validator(File xmlFile, boolean validarGalgo) {
        this.xmlFile = xmlFile;
        this.validarGalgo = validarGalgo;
    }

    /**
     *
     * @return Tipo de erros gerados. Este tipo reflete se a validação que gerou o erros foi de XSD
     *         ou de regras ANBIMA/Galgo.
     */
    public ErrorType getErrorType() {
        if (errorType == null) {
            log.debug("ErrorType nulo. Validando o arquivo XML.");
            validate_();
        }
        return errorType;
    }

    /**
     * Retorna a lista de erros encontrados na validação. Se validação não ocorreu (errors == null),
     * a validação é invocada antes.
     *
     * @return Lista de erros encontrados.
     */
    public List<String> getErrors() {
        if (errors == null) {
            log.debug("Lista de erros nula. Validando o arquivo XML.");
            validate_();
        }
        return errors;
    }

    /**
     * Realiza a validação do arquivo XML. A ordem de execução geral é:
     *
     * <ul>
     * <li>Primeiramente é realizada uma validação do esquema (XSD). Se houver erros,
     *     retorna esses erros e encerra a validação.</li>
     * <li>Se não houver erros de esquema, são validadas as regras ANBIMA/Galgo.
     * </ul>
     *
     * As classes Validator são todas invocadas, cada uma com uma regra de validação própria.
     * Se ocorrer algum erro ao se tentar instanciar uma classe Validator, um log é gerado
     * e o processo prossegue para a próxima classe Validator.
     *
     * @return Lista com os erros encontrados.
     */
    public List<String> validate_() {
        errors = Lists.newArrayList();

        // Validando contra o XSD
        errors.addAll(new SchemaValidator().validate(xmlFile));

        // Se houver erros de Schema, retorno sem validar o negócio
        if (!errors.isEmpty()) {
            errorType = ErrorType.XSD;
            return errors;
        }

        log.info("Executando as validações de negócio...");
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        if (xmlImporter.isGalgoAssBalStmt()) {
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                    importXml();
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            for (BsnsMsgComplexType bsnsMsg : bsnsMsgList) {
                busValidate(bsnsMsg.getDocument(), errors);
            }
        } else {
            Document document = (Document) xmlImporter.importXml();
            busValidate(document, errors);
        }

        if (validarGalgo) {
            log.info("Executando as validações Galgo no arquivo");
            if (xmlImporter.isGalgoAssBalStmt()) {
                GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                        importXml();
                List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
                for (BsnsMsgComplexType bsnsMsg : bsnsMsgList) {
                    busValidateGalgo(bsnsMsg.getDocument(), errors);
                }
            } else {
                Document document = (Document) xmlImporter.importXml();
                busValidateGalgo(document, errors);
            }
        }

        log.info("Fim das validações");
        errorType = ErrorType.ANBIMA;
        return errors;
    }

    /**
     * Valida o arquivo utilizando as classes Validator. Se ocorrer algum erro ao se tentar
     * instanciar a classe do tipo Validator, jogo o erro no log e continuo com a próxima classe
     * Validator.
     *
     * @param xmlFile
     * @param validarGalgo Inidica se as validações do Galgo devem ser feitas
     * <p>
     * @return
     */
    public static List<String> validate(File xmlFile, boolean validarGalgo) {
        return new Validator(xmlFile, validarGalgo).validate_();
    }

    /**
     * Validações do Layout definidas pela ANBIMA
     *
     * @param document
     * @param errors
     * <p>
     * @return
     */
    private static List<String> busValidate(Document document, List<String> errors) {
        for (Class<Validator_> validator : validatorClasses) {

            // Tento utilizar a classe validator. Se der algum erro, jogo no log
            // e continuo com a próxima classe
            try {
                errors.addAll(validator.newInstance().validate(document));
            } catch (Exception e) {
                log.error(null, e);
            }
        }
        return errors;
    }

    /**
     * Validações específicas do Sistema Galgo
     *
     * @param document
     * @param errors
     * <p>
     * @return
     */
    private static List<String> busValidateGalgo(Document document, List<String> errors) {
        for (Class<Validator_> validator : galgoValidatorClasses) {

            // Tento utilizar a classe validator. Se der algum erro, jogo no log
            // e continuo com a próxima classe
            try {
                errors.addAll(validator.newInstance().validate(document));
            } catch (Exception e) {
                log.error(null, e);
            }
        }
        return errors;
    }

    /**
     * As classes Validator são inicializadas e carregadas em uma lista de classes validadoras.
     * Esta lista será percorrida no momento da validação do arquivo XML, sendo a validação de
     * cada classe invocada.
     */
    private static void initialize() {
        try {
            log.info("Inicializando a classe Validator");
            ClassPath cp = ClassPath.from(Validator.class.getClassLoader());
            ImmutableSet<ClassPath.ClassInfo> busClasses = cp.getTopLevelClasses(BUS_PCKG);

            validatorClasses = Lists.newArrayList();
            for (ClassPath.ClassInfo busClass : busClasses) {
                Class<Validator_> clazz = (Class<Validator_>) busClass.load();
                if (clazz.getAnnotation(ValidatorType.class) != null) {
                    validatorClasses.add(clazz);
                }
            }

            galgoValidatorClasses = Lists.newArrayList();
            for (ClassPath.ClassInfo busClass : busClasses) {
                Class<Validator_> clazz = (Class<Validator_>) busClass.load();
                if (clazz.getAnnotation(GalgoValidatorType.class) != null) {
                    galgoValidatorClasses.add(clazz);
                }
            }

            log.debug(validatorClasses.size()
                    + " classe(s) encontrada(s) com anotação ValidatorType");
            for (Class validatorClass : validatorClasses) {
                log.debug(validatorClass.getName());
            }
            log.info("Classes Validator inicializadas com sucesso");
        } catch (Exception ex) {
            log.error(null, ex);
            throw new RuntimeException(ex);
        }
    }
}
