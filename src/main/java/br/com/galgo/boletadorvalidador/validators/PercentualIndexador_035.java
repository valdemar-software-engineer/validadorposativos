/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * (Percentual do Indexador -  Índex Rate Basis)
 * - No caso de operações compromissadas pré fixados, este campo não será preenchido.
 *
 * Verificar para os seguintes tipos de ativos:
 *
 * Compromissadas;
 * Debentures;
 * Termo RF;
 * Títulos Publicos;
 * Títulos Privados;
 *
 * @author joyce.oliveira
 */
@ValidatorType
class PercentualIndexador_035 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PercentualIndexador_035.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Percentual Indexador");

        errors = Lists.newArrayList();

        boolean verificaPercentualIndexador = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;
        BigDecimal pencetualIndexador = null;

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Pencentual do Indexador
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Percentua do Indexador
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaPercentualIndexador = false;

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar se informou OFFR
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaPercentualIndexador = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <cd>");
                }

                //Se tipo de ativo obriga informar o percentual
                //Verificar se informou a tag <IndxRateBsis>
                if (verificaPercentualIndexador) {
                    try {
                        pencetualIndexador = listaAtivos.getFinInstrmAttrbts().getIndxRateBsis();
                        if (pencetualIndexador.equals(null)) {
                        }
                    } catch (NullPointerException e) {
                        errors.
                                add(" Obrigatório informar Percentual de Indexador para tipo de ativo"
                                + " de Código CVM igual a " + codigoTipoCVM + ". "
                                + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + " <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls><BalForSubAcct>"
                                + "<FinInstrmAttrbts><IndxRateBsis>");
                    }
                }
            }
        }
        return errors;
    }
}
