/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import com.galgo.utils.PropertyUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification20;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import jodd.bean.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * Detalhes da Carteira (BalanceForAccount)/(BalanceBreakdown)
 * <p/>
 * Quando sessão apresentar código RECE, o emissor deve ser igual vao valor da propriedade "ISO_20022"
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValoresReceber_016 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(ValoresReceber_016.class);
    private List<String> errors = Lists.newArrayList();

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste de Valores a Receber");

        errors = Lists.newArrayList();

        /**
         * Verificar Valores a Receber RECE
         *
         * Deve conferir se Valores a Receber = soma dos valores a receber informados
         */
        List<AggregateBalanceInformation13> balForAcctList = doc.getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 balForAcct : balForAcctList) {
            List<SubBalanceInformation6> balBrkdwnLst = balForAcct.getBalBrkdwn();

            for (SubBalanceInformation6 balBrkdwn : balBrkdwnLst) {
                //Verificar se é RECE:

                try {
                    //final GenericIdentification20 subBalTp_Prtry = balBrkdwn.getSubBalTp().getPrtry();
                    final GenericIdentification20 subBalTp_Prtry = (GenericIdentification20) PropertyUtils
                            .getPropertyDN(balBrkdwn, "subBalTp.prtry");
                    final String codigoTipo = subBalTp_Prtry.getId();
                    final String emissor = subBalTp_Prtry.getIssr();
                    //codigoTipo = balBrkdwn.getSubBalTp().getPrtry().getId();

                    //Se for igual a RECE, somar os valores
                    if ("RECE".equals(codigoTipo)) {
                        //Conferindo se Emissor = ISO 20022
                        if (!Config.getString("ISO_20022").equals(emissor)) {
                            errors.add("Quando indicar Valores a Receber, RECE, é obrigatório "
                                    + "informar o Emissor igual a " + Config.getString("ISO_20022") + ". Foi "
                                    + "informado " + emissor + ". <Issr><Prtry><SubBalTp><BalBrkdwn>"
                                    + "<BalForAcct>.");
                        }

                        // Valor financeiro total a receber
                        BigDecimal valorFinTotalReceber = (BigDecimal) BeanUtil.getPropertySilently(balBrkdwn,
                                "qty.qty.faceAmt");

                        List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = balBrkdwn.
                                getAddtlBalBrkdwnDtls();

                        BigDecimal totalizacaoRecebiveis = BigDecimal.ZERO;

                        for (AdditionalBalanceInformation6 addtlBalBrkdwnDtls : addtlBalBrkdwnDtlsLst) {
                            BigDecimal despesa = addtlBalBrkdwnDtls.getQty().getQty().getFaceAmt();
                            if (despesa != null) {
                                totalizacaoRecebiveis = totalizacaoRecebiveis.add(despesa);
                            }
                        }

                        // Validando se a totalização é igual ao total declarado (com Big decimal é
                        // necessário usar compareTo porque new BigDecimal(0.0f).equals(BigDecimal.ZERO) dá falso
                        if (totalizacaoRecebiveis.compareTo(valorFinTotalReceber) != 0) {
                            String errMsg = "Valor Financeiro Total a Receber, identidicado por RECE, deve ser igual à "
                                    + "soma dos valores a receber informados. Valor Financeiro"
                                    + " Total a Receber informado igual a " + valorFinTotalReceber
                                    + ", e a somatória dos recebíveis informados igual a " + totalizacaoRecebiveis
                                    + ". <BalForAcct><BalBrkdwn><Qty><Qty><FaceAmt>";
                            log.debug(errMsg);
                            errors.add(errMsg);
                        }

                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem id, verifica o próximo");
                }
            }
        }
        return errors;
    }

}
