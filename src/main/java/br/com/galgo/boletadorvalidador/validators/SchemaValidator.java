/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.List;

/**
 * @author valdemar.arantes
 */
class SchemaValidator {

    private static final Logger log = LoggerFactory.getLogger(SchemaValidator.class);
    /*private static final String XSD_POS_ATIVOS_PATH;
    private static final String XSD_POS_ATIVOS_ISO_PATH;*/
    private static final String XSD_POS_ATIVOS_URL;
    private static final String XSD_POS_ATIVOS_ISO_URL;

    static {
        String XSD_POS_ATIVOS = "SchemaPosicaoAtivos_EDITADO.xsd";
        String XSD_POS_ATIVOS_ISO = "Galgo_Posicao_de_Ativos_XSD_Envio_e_Cancelamento_semt.003.001.04_EDITADO.xsd";
        /*XSD_POS_ATIVOS_PATH = "etc/" + XSD_POS_ATIVOS;
        XSD_POS_ATIVOS_ISO_PATH = "etc/" + XSD_POS_ATIVOS_ISO;*/
        XSD_POS_ATIVOS_URL = "wsdl/" + XSD_POS_ATIVOS;
        XSD_POS_ATIVOS_ISO_URL = "wsdl/" + XSD_POS_ATIVOS_ISO;
    }

    private List<String> errors = Lists.newArrayList();

    public List<String> getErrors() {
        return errors;
    }

    public List<String> validate(Document document) {
        try {
            JAXBContext ctx = JAXBContext.newInstance(Document.class);
            JAXBSource source = new JAXBSource(ctx, Document.class);
            Schema schema = getSchemaPosAtivos();
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.setErrorHandler(new MyErrorHandler(errors));
            validator.validate(source);
            return errors;
        } catch (Exception e) {
            throw new RuntimeException("Erro ao validar o objecto Document a partir do XSD", e);
        }
    }

    public List<String> validate(GalgoAssBalStmtComplexType galgoAssBalStmt) {
        try {
            JAXBContext ctx = JAXBContext.newInstance(GalgoAssBalStmtComplexType.class);
            JAXBSource source = new JAXBSource(ctx, GalgoAssBalStmtComplexType.class);
            Schema schema = getSchemaPosAtivos();
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.setErrorHandler(new MyErrorHandler(errors));
            validator.validate(source);
            return errors;
        } catch (Exception e) {
            throw new RuntimeException("Erro ao validar o objecto Document a partir do XSD", e);
        }
    }

    public List<String> validate(File xmlFile) {
        try {
            log.debug("Validando o schema do arquivo {}", xmlFile.getCanonicalFile());
            javax.xml.validation.Validator validator = getSchemaPosAtivos().newValidator();
            log.debug("javax.xml.validation.Validator location: {}", Thread.currentThread().
                    getContextClassLoader().getResource("javax/xml/validation/Validator.class"));
            URL url = validator.getClass().getResource(validator.getClass().getSimpleName() + ".class");
            log.debug("XML Validator implementation location: {}", url);

            // preparing the XML file as a SAX source
            SAXSource source = new SAXSource(new InputSource(new FileInputStream(xmlFile)));

            // validating the SAX source against the schema
            validator.setErrorHandler(new MyErrorHandler(errors));
            try {
                validator.validate(source);
            } catch (SAXParseException e) {
                errors.add("Erro ao realizar o parse do arquivo XML: " + e.toString());
            }
            return errors;
        } catch (SAXParseException e) {
            throw new RuntimeException("Erro ao realizar o parse do arquivo XML: " + e.toString(), e);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao validar o arquivo a partir do XSD: " + e.
                    getMessage(), e);
        } finally {
            log.debug("Fim da validação com o schema");
        }
    }

    /**
     * @return Schema de Posição de Ativos
     * @throws SAXException
     */
    private Schema getSchemaPosAtivos() throws SAXException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        URL xsdURL = this.getClass().getClassLoader().getResource(XSD_POS_ATIVOS_URL);
        log.debug("Schema definido no arquivo {}", xsdURL);
            /*File xsdFile = new File(XSD_POS_ATIVOS_PATH);
            log.debug("Schema definido no arquivo {}", xsdFile.getCanonicalPath());
            Schema schema = sf.newSchema(xsdFile);*/
        //Source xsdSource = new StreamSource(this.getClass().getClassLoader().getResourceAsStream(XSD_POS_ATIVOS));
        //Schema schema = sf.newSchema(xsdSource);
        return sf.newSchema(xsdURL);
    }

    private static class MyErrorHandler extends DefaultHandler {

        private final List<String> errors;

        private MyErrorHandler(List<String> errors) {
            this.errors = errors;
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        @Override
        public void warning(SAXParseException e) throws SAXException {
            log.warn("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber() + "; Descrição: " + e
                    .getMessage());
            printInfo(e);
        }

        private void printInfo(SAXParseException e) {
            errors.add("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber() + "; Descrição: " + e
                    .getMessage());
        }
    }
}
