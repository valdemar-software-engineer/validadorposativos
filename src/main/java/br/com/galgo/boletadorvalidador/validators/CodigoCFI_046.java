/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CodigoCFI_046 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CodigoCFI_046.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Código CFI");

        errors = Lists.newArrayList();

        boolean verificaCFI;
        String tipoAtivo;
        String codigoTipoCVM = null;
        String codCFI = null;

        //Código CVM dos ativos que precisam ter o código CFI informado
        List<String> listaCodigosCVM = new ArrayList<>();
        listaCodigosCVM.add("75");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("37");
        listaCodigosCVM.add("197");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");
        listaCodigosCVM.add("102");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaCFI = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaCFI = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (!verificaCFI) {
                    return errors;
                }

                //Cerificando se Código CFI tem 6 caracteres
                try {
                    codCFI = listaAtivos.getFinInstrmAttrbts().getClssfctnTp().
                            getClssfctnFinInstrm();

                    if (codCFI.length() < 6) {
                        errors.add("O código CFI deve ter seis caracteres. Foi informado o "
                                + "código " + codCFI + ". <SubAcctDtls><BalForSubAcct>"
                                + "<FinInstrmAttrbts><ClssfctnTp><ClssfctnFinInstrm>");
                    }

                } catch (NullPointerException e) {
                    errors.add("Obrigatório informar Código CFI para ativo do tipo CVM "
                            + "igual a " + codigoTipoCVM + ". <SubAcctDtls><BalForSubAcct>"
                            + "<FinInstrmAttrbts><ClssfctnTp><ClssfctnFinInstrm>");
                }

                if (verificaCFI) {
                    //Título Publico
                    try {
                        if (codigoTipoCVM.equals("75")) {
                            //Primeiro caracter deve ser igual a D
                            if (!"D".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a D. Foi "
                                        + "informado " + codCFI.charAt(0) + ".<SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Quarto caracter deve ser igual a T
                            try {
                                if (!"T".equals(String.valueOf(codCFI.charAt(3)))) {
                                    errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM
                                            + " deve "
                                            + "ter quarto caracter do código CFI igual a T. Foi "
                                            + "informado " + codCFI.charAt(3) + ".<SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                            + "<ClssfctnFinInstrm>");
                                }
                            } catch (IndexOutOfBoundsException e) {
                                errors.add("Código CFI deve ter seis caracteres, foi informado o código "
                                        + codCFI + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts>"
                                        + "<ClssfctnTp><ClssfctnFinInstrm>");
                            }
                        }

                        //Título Privado
                        if (codigoTipoCVM.equals("196")) {
                            //Primeiro caracter deve ser igual a D
                            if (!"D".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a D. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Quarto caracter deve ser igual a G
                            try {
                                if (!"G".equals(String.valueOf(codCFI.charAt(3)))) {
                                    errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM
                                            + " deve "
                                            + "ter o quarto caracter do código CFI igual a G. Foi "
                                            + "informado " + codCFI.charAt(3) + ". <SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                            + "<ClssfctnFinInstrm>");
                                }
                            } catch (IndexOutOfBoundsException e) {
                                errors.add("Código CFI deve ter seis caracteres, foi informado o código "
                                        + codCFI + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts>"
                                        + "<ClssfctnTp><ClssfctnFinInstrm>");
                            }
                        }

                        //Debentures
                        if (codigoTipoCVM.equals("193")) {
                            //Primeiro caracter deve ser igual a D
                            if (!"D".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a D. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Quarto caracter deve ser igual a U
                            if (!"U".equals(String.valueOf(codCFI.charAt(3)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter o quarto caracter do código CFI igual a U. Foi "
                                        + "informado " + codCFI.charAt(3) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                        }

                        //Ações
                        if (codigoTipoCVM.equals("37")) {
                            //Primeiro caracter deve ser igual a E
                            if (!"E".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a E. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Segundo caracter deve ser igual a “S”, “P”, “R”, “C”, “F” ou “V”.“S”,
                            //“P”, “R”, “C”, “F” ou “V”
                            List<String> listaCaracteres = new ArrayList<>();
                            listaCaracteres.add("S");
                            listaCaracteres.add("P");
                            listaCaracteres.add("R");
                            listaCaracteres.add("C");
                            listaCaracteres.add("F");
                            listaCaracteres.add("V");
                            listaCaracteres.add("S");

                            try {
                                if (!listaCaracteres.contains(String.valueOf(codCFI.charAt(3)))) {
                                    errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve"
                                            + " ter o quarto caracter igual a S, P, R, C, F, V ou S. "
                                            + "Foi informado " + codCFI.charAt(3)+". <SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                            + "<ClssfctnFinInstrm>");
                                }
                            } catch (IndexOutOfBoundsException e) {
                                errors.add("Código CFI deve ter seis caracteres, foi informado o código "
                                        + codCFI + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts>"
                                        + "<ClssfctnTp><ClssfctnFinInstrm>");
                            }
                        }

                        //Cotas
                        if (codigoTipoCVM.equals("197")) {
                            //Primeiro caracter deve ser igual a E
                            if (!"E".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a E. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Segundo caracter deve ser igual a U
                            try {
                                if (!"U".equals(String.valueOf(codCFI.charAt(1)))) {
                                    errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM
                                            + " deve "
                                            + "ter o quarto caracter do código CFI igual a U. Foi "
                                            + "informado " + codCFI.charAt(1) + ". <SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                            + "<ClssfctnFinInstrm>");
                                }
                            } catch (IndexOutOfBoundsException e) {
                                errors.add("Código CFI deve ter seis caracteres, foi informado o código "
                                        + codCFI + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts>"
                                        + "<ClssfctnTp><ClssfctnFinInstrm>");
                            }
                        }

                        //Opções
                        if (codigoTipoCVM.equals("39") || codigoTipoCVM.equals("40")) {
                            //Primeiro caracter deve ser igual a O
                            if (!"O".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a O. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                        }

                        //Futuros
                        if (codigoTipoCVM.equals("102")) {
                            //Primeiro caracter deve ser igual a F
                            if (!"F".equals(String.valueOf(codCFI.charAt(0)))) {
                                errors.add("Ativo do tipo CVM igual a " + codigoTipoCVM + " deve "
                                        + "ter primeiro caracter do código CFI igual a F. Foi "
                                        + "informado " + codCFI.charAt(0) + ". <SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><ClssfctnTp>"
                                        + "<ClssfctnFinInstrm>");
                            }
                            //Segundo caracter deve ser igual a
                        }
                    } catch (NullPointerException e) {
                    }

                    //TODO - Aguardar definicoes Carteiras Imobiliarias

                }
            }
        }
        return errors;
    }
}
