/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * O erro PA.0453 ocorre quando a tag “SctiesBalAcctgRpt/BalForAcct/PricDtls”
 * for informada mais que uma vez. Deve existir somente um Tipo de Cota.
 *
 * @author joyce.oliveira
 */
@GalgoValidatorType
class PrecoDuplicado_107 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PrecoDuplicado_107.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Detalhe Preco Invalido");

        errors = Lists.newArrayList();

        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 listaFundos : balForAcctList) {
            List<PriceInformation5> pricDtlsLst = listaFundos.getPricDtls();

             //Capturando a identificação do ativo
	Identificador identifica = new Identificador();
	SecurityIdentification14 listaIdentificadoresAtivo = listaFundos.getFinInstrmId();
	String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

    for(PriceInformation5 listaPrecos : pricDtlsLst){
                String codigoDetalhePreco = listaPrecos.getTp().getCd().toString();

                if(!"NAVL".equals(codigoDetalhePreco)){
                    errors.add("Validação do Sistema Galgo: PA.0453 - Tipo do Detalhe do "
                            + "Preço informado deve ser igual a “NAVL“, foi "
                            + "informado "+codigoDetalhePreco
                            + ". <SctiesBalAcctgRpt><BalForAcct><PricDtls><Tp><Cd>");
                }
            }


        }
        return errors;
    }
}
