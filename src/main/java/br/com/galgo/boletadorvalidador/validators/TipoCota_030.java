/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceValueType1Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Subseção (<PricDtls><Tp><Cd>) para informar o tipo de cota do Fundo. No Brasil,
 * a identificação será NAVL - Net Asset Value
 * Subseção (<PricDtls><ValTp><ValTp>) complementar para informar qual o tipo da cota.
 * No Brasil informaremos sempre PARV Valor.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class TipoCota_030 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            TipoCota_030.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Tipo de Cota");

        errors = Lists.newArrayList();

        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 listaFundos : balForAcctList) {

            List<PriceInformation5> prcInfList = listaFundos.getPricDtls();

            //Capturando a identificação do fundo
            Identificador identifica = new Identificador();
            SecurityIdentification14 listaIdentificadoresAtivo = listaFundos.getFinInstrmId();
            String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

            for (PriceInformation5 listaPrecos : prcInfList) {
                //Verificar se <PricDtls><Tp><Cd> igual a NAVL
                TypeOfPrice11Code tipoCota = listaPrecos.getTp().getCd();

                if (tipoCota == null) {
                    errors.add("Para o mercado brasileiro é obrigatório informar o valor"
                            + " NAVL para o tipo de cota na tag <Cd>. "
                            + " Verificar fundo de código " + id[1] + " igual a " + id[0] + ". "
                            + "<SctiesBalAcctgRpt><BalForAcct><PricDtls><Tp><Cd>");
                } else {
                    String tpCota = tipoCota.toString();

                    if (!tpCota.equals("NAVL")) {
                        errors.add("Para o mercado brasileiro é obrigatório informar o valor NAVL, "
                                + "foi informado " + tpCota + ". "
                                + " Verificar fundo de código " + id[1] + " igual a " + id[0] + ". "
                                + " <SctiesBalAcctgRpt><BalForAcct><PricDtls>"
                                + "<Tp><Cd>");
                    }
                }
                //Verificar se <PricDtls><ValTp><ValTp> igual a PARV
                PriceValueType1Code tipoValorCota = null;

                tipoValorCota = listaPrecos.getValTp().getValTp();
                if (tipoValorCota == null) {
                    errors.add("Para o mercado brasileiro é obrigatório informar o valor "
                            + "PARV para o tipo de valor da cota na tag <ValTp>. "
                            + "Verificar fundo de código " + id[1] + " igual a " + id[0] + ". "
                            + "<SctiesBalAcctgRpt><BalForAcct><PricDtls><ValTp><ValTp>");

                } else {

                    String tpVlrCota = tipoValorCota.toString();

                    if (!tpVlrCota.equals("PARV")) {
                        errors.add("Para o mercado brasileiro é obrigatório informar o valor"
                                + "PARV para o tipo de valor da cota, foi informado " + tpVlrCota
                                + "."
                                + " Verificar ativo de código " + id[1] + " igual a " + id[0] + ". "
                                + "<SctiesBalAcctgRpt><BalForAcct><PricDtls><ValTp><ValTp>");
                    }
                }
            }
        }

        return errors;
    }
}
