/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import java.util.List;

/**
 *
 * @author joyce.oliveira
 */
public class TipoCVM {

       public List<String> confereTipoCVM(List<AggregateBalanceInformation13> listaIdsFundos,
            List<String> listaCodigosProcurados) {
        String id;
        String cod;
        List <String> tipoAtivo = null;

  for (AggregateBalanceInformation13 listaAtivos : listaIdsFundos) {
   List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().getOthrId();

   for (OtherIdentification1 listaIdAtivos : othrIdLst) {
            id = listaIdAtivos.getTp().getPrtry();
            if ("CVM CDA 3.0 Tabela B".equals(id)) {
                cod = listaIdAtivos.getId();
                if (listaCodigosProcurados.contains(cod)) {
                    tipoAtivo.add(cod);

                } else{
                    tipoAtivo.add("0");
                }
            }
        }
       }
        return null;

       }
    }
