/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Precisa ser tratado o caso de mais de um elemento BalForAcct.
 * Será utilizado apenas o primeiro da lista.
 *
 * @author valdemar.arantes
 */
public class BalanceForAccountUtils extends BalanceForAccountUtilsAbstract {

    private static final Logger log = LoggerFactory.getLogger(BalanceForAccountUtils.class);

    public BalanceForAccountUtils(AggregateBalanceInformation13 balForAcct) {
        super(balForAcct);
    }

    /**
     *
     * @return Valor da cota do fundo/carteira
     */
    public BigDecimal getValorCota() {
        BigDecimal valorCota = null;
        try {
            List<PriceInformation5> pricDtlsList = balForAcct.getPricDtls();
            for (PriceInformation5 pricDtls : pricDtlsList) {
                try {
                    TypeOfPrice11Code code = pricDtls.getTp().getCd();
                    if (TypeOfPrice11Code.NAVL.equals(code)) {
                        valorCota = pricDtls.getVal().getAmt().getValue();
                    }
                } catch (NullPointerException e) {
                }
            }
        } catch (NullPointerException e) {
            log.warn("PriceDtls não encontrado em balForAcct.getPricDtls()");
        }
        return valorCota;
    }

    /**
     *
     * @return Quantidade de cotas do fundo/carteira
     */
    public BigDecimal getQtdCotas() {
        BigDecimal qtdCotas = null;
        try {
            qtdCotas = balForAcct.getAggtBal().getQty().getQty().getQty().getUnit();
        } catch (NullPointerException e) {
            log.warn("Qtd. de cotas não encontrado em "
                    + "balForAcct.getAggtBal().getQty().getQty().getQty().getUnit()");
        }
        return qtdCotas;
    }
}
