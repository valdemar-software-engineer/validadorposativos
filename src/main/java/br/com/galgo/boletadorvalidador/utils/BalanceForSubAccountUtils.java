/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import com.galgo.utils.PropertyUtils;
import com.google.common.collect.Maps;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.IdentificationSource3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jodd.bean.BeanUtil;
import org.apache.commons.beanutils.BeanPropertyValueEqualsPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author valdemar.arantes
 */
public class BalanceForSubAccountUtils extends BalanceForAccountUtilsAbstract {

    private static final Logger log = LoggerFactory.getLogger(BalanceForSubAccountUtils.class);
    private final AggregateBalanceInformation13 balForSubAcct;
    private String tpANBIMA;
    private boolean tpANBIMAPopulated = false;

    public BalanceForSubAccountUtils(AggregateBalanceInformation13 balForSubAcct) {
        super(balForSubAcct);
        this.balForSubAcct = balForSubAcct;
    }

    /**
     * @return Código de identificação ANBIMA do ativo
     */
    public String getCodIdentAtivo() {

        if (tpANBIMAPopulated) {
            return tpANBIMA;
        }
        tpANBIMAPopulated = true;

        Object othrIdList = PropertyUtils.getPropertyDN(balForSubAcct, "finInstrmId.othrId");
        if (CollectionUtils.isNotEmpty((Collection) othrIdList)) {
            try {
                Object othrId = CollectionUtils.find((Collection) othrIdList,
                        new BeanPropertyValueEqualsPredicate("tp.prtry",
                                Config.getString("tabela_identicacao_ativos")));
                tpANBIMA = (String) PropertyUtils.getPropertyDN(othrId, "id");
            } catch (Exception e) {
            }
        }
        return tpANBIMA;
    }

    /**
     * Atribui um código ANBIMA do ativo, caso o ativo não tenha código definido.
     * <p>
     * @param tpANBIMA
     * <p>
     * @return True - se o valor for atribuído com sucesso.
     * <br/>False - caso contrário.
     */
    public boolean setCodIdentAtivo(String tpANBIMA) {

        if (StringUtils.isBlank(getCodIdentAtivo())) {
            //List<OtherIdentification1> a = balForSubAcct.getFinInstrmId().getOthrId();
            List othrIdList = (List) PropertyUtils.
                    getPropertyDN(balForSubAcct, "finInstrmId.othrId");
            OtherIdentification1 othrId = new OtherIdentification1();

            Map<String, String> props = Maps.newHashMap();
            props.put("tp.prtry", Config.getString("tabela_identicacao_ativos"));
            props.put("id", tpANBIMA);

            for (Map.Entry<String, String> entrySet : props.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue();
                BeanUtil.setPropertyForced(othrId, key, value);
            }

            if (CollectionUtils.isNotEmpty(othrIdList)) {
                othrIdList.add(othrId);
            } else {
                BeanUtil.setPropertyForced(balForSubAcct, "finInstrmId.othrId[0]", othrId);
            }

            // È necessário invocar o refresh para atualizar os valores por conta da alteração
            // realizada nas linhas acima
            refresh();
            return true;
        } else {
            return false;
        }
    }

    private void refresh() {
        refreshId();
        tpANBIMAPopulated = false;
    }

}
