/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.IdentificationSource3Choice;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import java.util.ArrayList;
import java.util.List;
import jodd.bean.BeanUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Precisa ser tratado o caso de mais de um elemento BalForAcct.
 * Será utilizado apenas o primeiro da lista.
 *
 * @author valdemar.arantes
 */
public class BalanceForAccountUtilsAbstract {

    private static final Logger log = LoggerFactory.getLogger(BalanceForAccountUtilsAbstract.class);
    //private final Document doc;
    protected final AggregateBalanceInformation13 balForAcct;
    private final List<Id> idList = new ArrayList();
    private boolean idPopulated = false;

    public static class Id {

        public String code;
        public String value;
    }

    public BalanceForAccountUtilsAbstract(AggregateBalanceInformation13 balForAcct) {
        this.balForAcct = balForAcct;
    }

    public String getIdAsString() {
        SecurityIdentification14 finInstrmId = balForAcct.getFinInstrmId();
        return ObjectUtils.getAsJsonString(finInstrmId, false);
    }

    public List<Id> getIdList() {
        return populateId();
    }

    protected List<Id> refreshId() {
        idPopulated = false;
        return populateId();
    }

    /**
     * Popula o atributo de instância id apenas na primeira execução. Nas demais utiliza o id
     * populado.
     *
     * @return Lista dos IDs encontrados
     */
    protected List<Id> populateId() {

        // Controle de execução na primeira vez
        if (idPopulated) {
            return idList;
        }
        idPopulated = true;

        SecurityIdentification14 finInstrmId = balForAcct.getFinInstrmId();
        if (StringUtils.isNotBlank(finInstrmId.getISIN())) {
            Id id = new Id();
            id.code = "ISIN";
            id.value = finInstrmId.getISIN();
            idList.add(id);
        }

        List<OtherIdentification1> othrIdList = finInstrmId.getOthrId();
        if (othrIdList.isEmpty()) {
            return idList;
        }

        for (OtherIdentification1 othrId : othrIdList) {

            // Se não encontrar valor na tag othrId, vai para o próximo othrId
            String value = othrId.getId();
            if (StringUtils.isBlank(value)) {
                continue;
            }

            String code = null;
            IdentificationSource3Choice tp = othrId.getTp();
            // Se Tp não está definido, vai pro próximo othrId
            if (tp == null) {
                continue;
            }

            String cd = tp.getCd();
            if (StringUtils.isNotBlank(cd)) {
                Id id = new Id();
                id.value = value;
                id.code = cd;
                idList.add(id);
            }

            // Não encontrou nada no Tp/Cd, tentando no Tp/Prtry
            String prtry = tp.getPrtry();
            if (StringUtils.isNotBlank(prtry)) {
                Id id = new Id();
                id.value = value;
                id.code = prtry;
                idList.add(id);
            }
        } // Fim de <for (OtherIdentification1 othrId : othrIdList)>

        return idList;
    }

    /**
     * @return Código MIC da carteira / fundo. Retorna nulo se não estiver definido
     */
    public String getCodMIC() {
        return (String) BeanUtil.getPropertySilently(balForAcct, "finInstrmAttrbts.plcOfListg.id.mktIdrCd");
    }

    /**
     * Atribui um código MIC ao fundo ou carteira
     * 
     * @param codMIC
     */
    public void setCodMIC(String codMIC) {
        BeanUtil.setPropertyForced(balForAcct, "finInstrmAttrbts.plcOfListg.id.mktIdrCd", codMIC);
    }
}
