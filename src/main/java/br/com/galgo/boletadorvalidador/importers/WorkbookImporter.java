/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class WorkbookImporter {

    private static final Logger log = LoggerFactory.getLogger(WorkbookImporter.class);
    private File workbookFile;

    public WorkbookImporter(File workbookFile) {
        this.workbookFile = workbookFile;
    }

    public GalgoAssBalStmtComplexType importWorkbook() {
        InputStream inp = null;
        try {
            log.debug("Abrindo a planilha {}", workbookFile.getCanonicalPath());

            Workbook wb;
            try {
                inp = new FileInputStream(workbookFile);
                wb = new HSSFWorkbook(inp);
            } catch (OfficeXmlFileException e) {
                inp = new FileInputStream(workbookFile);
                wb = new XSSFWorkbook(inp);
            }
            return new SheetsImporter(wb).importSheets();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (inp != null) {
                try {
                    inp.close();
                } catch (Exception e) {
                }
            }
        }
    }
}
