/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.io.File;
import java.io.FileInputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Classe que importa o XML de posição de ativos executando o parse do XML. O parse do
 * arquivo só será executado pelo método importXml uma vez por instância em caso de
 * sucesso. Se ocorrer algum erro no parse, a próxima execução do método importXml tentará
 * o realizar o parse novamente.
 *
 * @author valdemar.arantes
 */
public class XmlImporter {

    private static final Logger log = LoggerFactory.getLogger(XmlImporter.class);
    private Boolean isGalgoAssBalStmt = null;
    private File xmlFile;

    public XmlImporter(File xmlFile) {
        this.xmlFile = xmlFile;
    }

    /**
     * Realiza o parse do arquivo. Se este arquivo já tiver sido parseado anteriormente
     * por uma mesma instância desta classe, o Document obtido no primeiro parse será
     * retornado.
     *
     * @return
     */
    public Object importXml() {

        try {
            SAXSource source = new SAXSource(new InputSource(new FileInputStream(xmlFile)));
            JAXBContext ctx = JAXBContext.newInstance(GalgoAssBalStmtComplexType.class);
            Unmarshaller unm = ctx.createUnmarshaller();

            if (isGalgoAssBalStmt()) {
                return unm.unmarshal(source, GalgoAssBalStmtComplexType.class).getValue();
            } else {
                return unm.unmarshal(source, Document.class).getValue();
            }
        } catch (Exception e) {
            log.error(null, e);
            throw new RuntimeException("Erro ao importar o arquivo XML");
        }
    }

    public Boolean isGalgoAssBalStmt() {
        if (isGalgoAssBalStmt != null) {
            return isGalgoAssBalStmt;
        }

        log.debug("Verificando qual a raiz do arquivo XML");
        XPath xpath = XPathFactory.newInstance().newXPath();
        InputSource inputSource;
        try {
            inputSource = new InputSource(new FileInputStream(xmlFile));
            NodeList nodes = (NodeList) xpath.evaluate("/", inputSource,
                    XPathConstants.NODESET);

            // A rotina abaixo recupera a próxima tag, descartando, por exemplo, os comentários
            NodeList childNodes = nodes.item(0).getChildNodes();
            Node root = null;
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node node = childNodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    root = node;
                    break;
                }
            }
            isGalgoAssBalStmt = "GalgoAssBalStmt".equals(root.getLocalName());
            return isGalgoAssBalStmt;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
