/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.ui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Window;
import javax.swing.JDialog;
import javax.swing.JProgressBar;

/**
 *
 * @author valdemar.arantes
 */
public class ProgressBarDialog extends JDialog {

    private Thread t;

    public ProgressBarDialog() {
    }

    public ProgressBarDialog(Frame owner) {
        super(owner);
    }

    public ProgressBarDialog(Frame owner, boolean modal) {
        super(owner, modal);
    }

    public ProgressBarDialog(Frame owner, String title) {
        super(owner, title);
    }

    public ProgressBarDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
    }

    public ProgressBarDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
        super(owner, title, modal, gc);
    }

    public ProgressBarDialog(Dialog owner) {
        super(owner);
    }

    public ProgressBarDialog(Dialog owner, boolean modal) {
        super(owner, modal);
    }

    public ProgressBarDialog(Dialog owner, String title) {
        super(owner, title);
    }

    public ProgressBarDialog(Dialog owner, String title, boolean modal) {
        super(owner, title, modal);
    }

    public ProgressBarDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) {
        super(owner, title, modal, gc);
    }

    public ProgressBarDialog(Window owner) {
        super(owner);
    }

    public ProgressBarDialog(Window owner, ModalityType modalityType) {
        super(owner, modalityType);
    }

    public ProgressBarDialog(Window owner, String title) {
        super(owner, title);
    }

    public ProgressBarDialog(Window owner, String title, ModalityType modalityType) {
        super(owner, title, modalityType);
    }

    public ProgressBarDialog(Window owner, String title, ModalityType modalityType,
            GraphicsConfiguration gc) {
        super(owner, title, modalityType, gc);
    }

    @Override
    protected void dialogInit() {
        super.dialogInit();
        JProgressBar dpb = new JProgressBar(0, 500);
        dpb.setIndeterminate(true);
        add(BorderLayout.CENTER, dpb);
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setSize(300, 55);
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            t = new Thread(new Runnable() {
                @Override
                public void run() {
                    ProgressBarDialog.this.setLocationRelativeTo(getOwner());
                    ProgressBarDialog.super.setVisible(true);
                }
            });
            t.start();

        } else {
            super.setVisible(false);
            t.interrupt();
        }
    }
}
