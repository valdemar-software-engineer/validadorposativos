/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.ui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author valdemar.arantes
 */
public class BoletValidFrame extends javax.swing.JFrame {

    /**
     * Creates new form BoletValidFrame
     */
    public BoletValidFrame() {
        initComponents();


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING:
     * Do NOT modify this code. The content of this method is always regenerated
     * by the
     * Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        validarButton = new javax.swing.JButton();
        excluiButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        adicionaButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaXMLSelecionadosTable = new javax.swing.JTable();
        selecionaXMLFileChooser = new javax.swing.JFileChooser();
        menuBar = new javax.swing.JMenuBar();
        Arquivo = new javax.swing.JMenu();
        boletadorMenuItem = new javax.swing.JCheckBoxMenuItem();
        validadorMenuItem = new javax.swing.JCheckBoxMenuItem();
        Ajuda = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Boletador Validador");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        validarButton.setText("Validar");
        validarButton.setToolTipText("Excluir arquivos");
        validarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validarButtonActionPerformed(evt);
            }
        });

        excluiButton.setText("-");
        excluiButton.setToolTipText("Excluir arquivos");
        excluiButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                excluiButtonActionPerformed(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        jTextField1.setText("jTextField1");

        adicionaButton.setText("+");
        adicionaButton.setToolTipText("Selecionar arquivos");
        adicionaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionaButtonActionPerformed(evt);
            }
        });

        tabelaXMLSelecionadosTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Caminho"
            }
        ));
        tabelaXMLSelecionadosTable.setColumnSelectionAllowed(true);
        tabelaXMLSelecionadosTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tabelaXMLSelecionadosTable);
        tabelaXMLSelecionadosTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(adicionaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(excluiButton, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(validarButton)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addGap(0, 20, Short.MAX_VALUE)
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(okButton, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 584, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(230, 230, 230))))))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adicionaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(excluiButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(validarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 221, Short.MAX_VALUE)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(okButton)
                .addContainerGap())
        );

        menuBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        Arquivo.setText("File");
        Arquivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ArquivoActionPerformed(evt);
            }
        });

        boletadorMenuItem.setText("Boletador");
        boletadorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boletadorMenuItemActionPerformed(evt);
            }
        });
        Arquivo.add(boletadorMenuItem);

        validadorMenuItem.setText("Validador");
        validadorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validadorMenuItemActionPerformed(evt);
            }
        });
        Arquivo.add(validadorMenuItem);

        menuBar.add(Arquivo);

        Ajuda.setText("Ajuda");
        menuBar.add(Ajuda);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 141, Short.MAX_VALUE)
                    .addComponent(selecionaXMLFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 141, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(76, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 67, Short.MAX_VALUE)
                    .addComponent(selecionaXMLFileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 68, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
            @Override
            protected String doInBackground() throws Exception {
                System.out.println("okButtonActionPerformed");
                try {
                    Thread.sleep(5000);
                    System.out.println("Fim da thread");
                    jTextField1.setText("Saindo");
                    return "Olá, mundo!";
                } catch (InterruptedException ex) {
                    Logger.getLogger(BoletValidFrame.class.getName()).log(
                            Level.SEVERE, null, ex);
                    return ex.getMessage();
                }
            }
        };
        worker.execute();
    }//GEN-LAST:event_okButtonActionPerformed

    private void ArquivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ArquivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ArquivoActionPerformed

    private void boletadorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boletadorMenuItemActionPerformed
        // TODO add your handling code here:
        if (boletadorMenuItem.isSelected() == true) {
            validadorMenuItem.setSelected(false);
            selecionaXMLFileChooser.setVisible(false);
            adicionaButton.setVisible(false);
            tabelaXMLSelecionadosTable.setVisible(false);

        }
    }//GEN-LAST:event_boletadorMenuItemActionPerformed

    private void validadorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validadorMenuItemActionPerformed
        // TODO add your handling code here:
        if (validadorMenuItem.isSelected() == true) {
            boletadorMenuItem.setSelected(false);
            adicionaButton.setVisible(true);
            tabelaXMLSelecionadosTable.setVisible(false);
        }
    }//GEN-LAST:event_validadorMenuItemActionPerformed

    private void adicionaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adicionaButtonActionPerformed
        // TODO add your handling code here:
        selecionaXMLFileChooser.setVisible(true);
        int returnVal = selecionaXMLFileChooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File arquivo = selecionaXMLFileChooser.getSelectedFile();
            Object[] campo = {"arquivo.xml", "Joyce"};

            // selecionaXMLFileChooser.

            // jTextField1.setText(arquivo.getName());
        }
    }//GEN-LAST:event_adicionaButtonActionPerformed

    private void excluiButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_excluiButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_excluiButtonActionPerformed

    private void validarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validarButtonActionPerformed
        // TODO add your handling code here:
        if (selecionaXMLFileChooser == null) {
        }
    }//GEN-LAST:event_validarButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel.
         * For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info
                    : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoletValidFrame.class.getName()).
                    log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoletValidFrame.class.getName()).
                    log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoletValidFrame.class.getName()).
                    log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoletValidFrame.class.getName()).
                    log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoletValidFrame().setVisible(true);

                
                
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Ajuda;
    private javax.swing.JMenu Arquivo;
    private javax.swing.JButton adicionaButton;
    private javax.swing.JCheckBoxMenuItem boletadorMenuItem;
    private javax.swing.JButton excluiButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton okButton;
    private javax.swing.JFileChooser selecionaXMLFileChooser;
    private javax.swing.JTable tabelaXMLSelecionadosTable;
    private javax.swing.JCheckBoxMenuItem validadorMenuItem;
    private javax.swing.JButton validarButton;
    // End of variables declaration//GEN-END:variables
}
