/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.exporters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;

/**
 * Classe utilizada para exportar o arquivo template.xls localizado na raiz do classpath
 *
 * @author valdemar.arantes
 */
public class TemplateExcelExporter {

    private File outFile;

    /**
     *
     * @param outFile Arquivo onde para onde será exportado o template.xls
     */
    public TemplateExcelExporter(File outFile) {
        this.outFile = outFile;
    }

    /**
     * Método que exporta o template.xls
     */
    public void exportFile() {
        OutputStream xlsOut = null;
        try {
            InputStream xlsIn = Thread.currentThread().getContextClassLoader().
                    getResourceAsStream("template.xls");
            xlsOut = new FileOutputStream(outFile);
            IOUtils.copy(xlsIn, xlsOut);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Erro ao criar o arquivo " + outFile.getPath(), e);
        } catch (IOException e) {
            throw new RuntimeException("Erro ao copiar o arquivo template para o arquivo "
                    + outFile.getPath(), e);
        } finally {
            try {
                xlsOut.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
